package name.khartn.ftspc.indexer.ini_and_vars;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import javax.xml.parsers.*;
import name.khartn.ftspc.indexer.DataBases.C3P0.ConnectionPool;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * Class for loading settings from the ini file to the public static variables.
 *
 * @author Arthur Khusnutdinov
 *
 */
public class Lector_Ini {

    /**
     * Method for loading settings from the ini file to the public static
     * variables.
     */
    private void prepareIniFile() {
        StringBuilder text = new StringBuilder();
        String NL = System.getProperty("line.separator");
        Scanner scanner = null;
        Writer out = null;
        try {
            scanner = new Scanner(new FileInputStream("../main_config.ini"), "UTF-8");
            while (scanner.hasNextLine()) {
                text.append(scanner.nextLine()).append(NL);
            }
            out = new OutputStreamWriter(
                    new FileOutputStream("../main_config.ini"), "UTF-8");
            out.write(text.toString().replace("\\", "/"));
        } catch (Exception ex) {
            Vars.logger.fatal("Error: ", ex);
        } finally {
            try {
                scanner.close();
                out.close();
            } catch (Exception ex) {
            }
        }
    }

    /**
     * The method of settings initialization.
     */
    public void configure() {
        try {
            prepareIniFile();

            Properties keeper = new Properties();
            SchedulerFactory sf = new StdSchedulerFactory();
            keeper.load(new FileInputStream("../main_config.ini"));

            Vars.mysqlHost = keeper.getProperty("mysql_host");
            Vars.mysqlPort = keeper.getProperty("mysql_port");
            Vars.mysqlDb = keeper.getProperty("mysql_db");
            Vars.mysqlUser = keeper.getProperty("mysql_user");
            Vars.mysqlPassword = keeper.getProperty("mysql_passwd");
            Vars.Mater_Lector = keeper.getProperty("Mater_Lector");
            Vars.Lector_Repository = keeper.getProperty("Lector_Repository");
            Vars.CronExpressionForIndexer = keeper.getProperty("CronExpressionForIndexer");
            Vars.dubl_remover_time = keeper.getProperty("dubl_remover_time");
            Vars.IndexerToUse = keeper.getProperty("IndexerToUse");
            Vars.Lucene_Repo = keeper.getProperty("Lucene_Repo");
            Vars.PathToTmpDir = keeper.getProperty("PathToTmpDir");
            Vars.max_threads = Integer.parseInt(keeper.getProperty("max_threads"));
            Vars.ConnectionPool.MaxPoolSize = Integer.parseInt(keeper.getProperty("MaxPoolSize"));
            Vars.ConnectionPool.MinPoolSize = Integer.parseInt(keeper.getProperty("MinPoolSize"));
            Vars.ConnectionPool.AcquireIncrement = Integer.parseInt(keeper.getProperty("AcquireIncrement"));

            PropertyConfigurator.configure(props());
            Vars.logger = Logger.getRootLogger();

            if (System.getProperty("os.name").toUpperCase().contains("WIN")) {
                if (System.getProperty("os.arch").contains("x86")) {
                    Vars.os = 1;
                }
                if (System.getProperty("os.arch").contains("amd64")) {
                    Vars.os = 2;
                }
            }

            loadParsersFromXML();

            if (Vars.PathToTmpDir == null) {
                File tmpDir = new File("");
                tmpDir = new File(tmpDir.getAbsolutePath() + "/temp");
                if (tmpDir.mkdirs()) {
                    Vars.logger.info("Temporary directory successfully created");
                } else {
                    Vars.logger.warn("Temporary directory was not created!");
                }
                Vars.PathToTmpDir = tmpDir.getAbsolutePath().replace("\\", "/");
                tmpDir = null;
            }
            
            ConnectionPool.MakePool();

//            Vars.sched = sf.getScheduler();
//            Vars.sched.start();
        } catch (Exception ex) {
            ex.printStackTrace();
            Vars.logger.fatal("Error: ", ex);
        }
    }

    private void loadParsersFromXML() {
        try {
            InputStream propertiesIn =
                    Lector_Ini.class.getResourceAsStream("listOfParsers.xml");

            if (propertiesIn == null) {
                throw new FileNotFoundException(
                        "${archive}/com/google/code/ftspc/lector/ini_and_vars/listOfParsers.xml");
            }

            DefaultHandler handler = new DefaultHandler() {
                @Override
                public void startElement(String uri, String localName,
                        String qName, Attributes attributes)
                        throws SAXException {
                    Map<String, String> parsersFromXML =
                            new HashMap<>();

                    for (int i = 0; i < attributes.getLength(); i++) {
                        parsersFromXML.put(attributes.getQName(i),
                                attributes.getValue(i));
                    }
                    Vars.parsersFromXML.put(
                            attributes.getValue(attributes.getLength() - 1),
                            parsersFromXML);
                }
            };

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(propertiesIn, handler);

        } catch (Exception ex) {
            Vars.logger.fatal("Error: ", ex);
            ex.printStackTrace();
        }
    }

    private static Properties props() {
        Properties props = new Properties();
        props.put("log4j.rootLogger", "INFO, R");
        props.put("log4j.appender.R",
                "org.apache.log4j.DailyRollingFileAppender");
        props.put("log4j.appender.R.File", "logs/Main.log");
        props.put("log4j.appender.R.Append", "true");
        props.put("log4j.appender.R.Threshold", "INFO");
        props.put("log4j.appender.R.DatePattern", "'.'yyyy-MM-dd");
        props.put("log4j.appender.R.layout", "org.apache.log4j.PatternLayout");
        props.put("log4j.appender.R.layout.ConversionPattern",
                //"%d{HH:mm:ss,SSS} %c - %m%n");
                //"[%5p] %d{yyyy-MM-dd mm:ss} (%F:%M:%L)%n%m%n%n");
                "[%5p] %d{yyyy-MM-dd mm:ss} %c (%F:%M:%L)%n%m%n");
        return props;
    }
}
