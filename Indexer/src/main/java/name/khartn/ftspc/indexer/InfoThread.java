package name.khartn.ftspc.indexer;

import java.util.Date;
import java.util.TimerTask;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class InfoThread extends TimerTask {

    @Override
    public void run() {
        try {
            if (Vars.isIndexerRunning) {
                Date currentTime = new Date();
                Long timeDifference = currentTime.getTime() - Vars.startTime.getTime();
                Double speed;
                timeDifference = timeDifference / 1000;
                speed = ((Vars.totalSizeOfProcessedFiles / (1024 * 1024)) / timeDifference);
                String speedStr = String.valueOf(speed);
                speedStr = speedStr.substring(0, speedStr.indexOf(".") + 3);
                System.out.println("Speed: " + speedStr + " MB/s");
            }
        } catch (Exception ex) {
        }
    }
}
