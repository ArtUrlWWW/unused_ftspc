package name.khartn.ftspc.indexer.indexers.Lucene;

import java.io.File;
import java.util.Date;
import name.khartn.ftspc.indexer.indexers.CommonFunctions;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import static name.khartn.ftspc.indexer.ini_and_vars.Vars.*;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * The main for class running Lucene indexing
 * @author Arthur Khusnutdinov
 */
public class LuceneTimerForFilesChecking implements Job {

    /**
     * The method of the timer.
     * @param context
     * @throws JobExecutionException
     */
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
//        JobKey jobKey = context.getJobDetail().getKey();
//        TriggerKey triggerKey = context.getTrigger().getKey();
//        System.out.println("SimpleJob says: " + jobKey + " executing at " + new Date());
//        System.out.println(triggerKey);

        if (!Vars.isIndexerRunning) {
            Vars.startTime = new Date();
            try {
                Vars.server_thread_status = false;
                Vars.isIndexerRunning = true;
                Vars.server.suspend();
                System.out.println("Transferring data via FTP suspended.");
                synchronized (this) {
                        wait(15000);
                    }

                Directory store = FSDirectory.open(new File(Vars.Lucene_Repo));
                IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_40, null);
                iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
                iwc.setRAMBufferSizeMB(256.0);
                Vars.writerForLucene = new IndexWriter(store, iwc);

                CommonFunctions localCommonFunctions = new CommonFunctions(
                        new File(Mater_Lector));
                localCommonFunctions.indexDocs_main();

                while (current_run_indexes > 0) {
                    synchronized (this) {
                        wait(5000);
                    }
                }

//                Vars.writerForLucene.optimize();
                Vars.writerForLucene.close();

                System.gc();
                System.out.println("Transferring data via FTP continues.");

                Vars.endTime = new Date();
                Vars.elapsed_time = Vars.endTime.getTime() - Vars.startTime.getTime();
                System.out.println("Elapsed time= " + (Vars.elapsed_time / 1000) + "seconds");

                Vars.startTime = null;
                Vars.endTime = null;
                Vars.elapsed_time = null;
                Vars.totalSizeOfProcessedFiles = 0.0;

                Vars.server_thread_status = true;
                Vars.isIndexerRunning = false;
                Vars.server.resume();

            } catch (Exception ex) {
                Vars.logger.fatal("Error: ", ex);
                Vars.isIndexerRunning = false;
            }
        } else {
            System.out.println("Indexer is already running.");
        }
    }
}
