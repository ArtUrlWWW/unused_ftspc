package name.khartn.ftspc.indexer.indexers;

import name.khartn.ftspc.indexer.indexers.Lucene.Analyzers.AnalyzerDetermination;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

/**
 * Class for add data to the Lucene index, or Sphinx
 * @author Arthur Khusnutdinov
 */
public class AddDataToIndex {

    private String lang = null;

    /**
     * Constructor of the AddDataToIndex class
     * @param lang - Language of the text
     */
    public AddDataToIndex(String lang) {
        this.lang = lang;
    }

    /**
     * The method for adding data to the Lucene index, or Sphinx
     * @param fileContent The contents of the file (file's text)
     * @param filePath The path to file
     * @param fileName File name 
     */
    public void doAddData(String fileContent, String filePath, String fileName) {
        try {

            if (Vars.writerForLucene != null) {
                Document doc = new Document();
                PerFieldAnalyzerWrapper fieldAnalyzer;

                if (lang == null) {
                    if (fileContent.length() < 1000) {
                        lang = Vars.TextCategorizerLocal.categorize(
                                fileContent.substring(0, fileContent.length()));
                    } else {
                        lang = Vars.TextCategorizerLocal.categorize(
                                fileContent.substring(0, 1000));
                    }
                }

                fieldAnalyzer = AnalyzerDetermination.class.newInstance().getAnalyzer(lang);                
                doc.add(new Field("fileName", fileName, Field.Store.YES,
                        Field.Index.ANALYZED));
                doc.add(new Field("filePath", filePath, Field.Store.YES,
                        Field.Index.ANALYZED));
                doc.add(new Field("fileContent", fileContent, Field.Store.YES,
                        Field.Index.ANALYZED));
                Vars.writerForLucene.addDocument(doc, fieldAnalyzer);

                fieldAnalyzer = null;
                doc = null;
                lang = null;
                fileContent = null;
                filePath = null;

            } else if (Vars.stmtForMySQL != null) {
                // Here is the code for Sphinx.
            }

        } catch (Exception ex) {
            Vars.logger.fatal("Error: ", ex);
        }
    }
}
