package name.khartn.ftspc.indexer.parsers.Archives.ZIP;

import java.io.*;

/**
 * Class for moving / copying files and folders
 * @author Arthur Khusnutdinov
 */
public class MoveDir {

    /**
     * The function for copying files.
     * @param source Path to source file.
     * @param dest The path where you want to copy the file.
     * @throws IOException
     */
    public void copyFile(File source, File dest) throws IOException {

        if (!dest.exists()) {
            dest.createNewFile();
        }
        InputStream in = null;
        OutputStream out = null;
        try {
            in = new FileInputStream(source);
            out = new FileOutputStream(dest);

            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
        } finally {
            in.close();
            out.close();
        }
    }

    /**
     * Function for copying directory.
     * @param sourceDir Path to source directory.
     * @param destDir The path where to copy the directory.
     * @throws IOException
     */
    public void copyDirectory(File sourceDir, File destDir) throws IOException {

        if (!destDir.exists()) {
            destDir.mkdir();
        }

        File[] children = sourceDir.listFiles();

        for (File sourceChild : children) {
            String name = sourceChild.getName();
            File destChild = new File(destDir, name);
            if (sourceChild.isDirectory()) {
                copyDirectory(sourceChild, destChild);
            } else {
                copyFile(sourceChild, destChild);
            }
        }
        if (!delete(sourceDir)) {
            throw new IOException("Unable to delete original folder");
        }
    }

    private boolean delete(File resource) throws IOException {
        if (resource.isDirectory()) {
            File[] childFiles = resource.listFiles();
            for (File child : childFiles) {
                delete(child);
            }
        }
        return resource.delete();
    }
}
