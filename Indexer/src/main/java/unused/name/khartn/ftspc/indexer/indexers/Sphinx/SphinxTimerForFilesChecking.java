package name.khartn.ftspc.indexer.indexers.Sphinx;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.TimerTask;
import name.khartn.ftspc.indexer.indexers.CommonFunctions;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import static name.khartn.ftspc.indexer.ini_and_vars.Vars.*;
import name.khartn.ftspc.indexer.parsers.Archives.ZIP.UnZip;

/**
 * Class for the check, unpack and insert unpacked text files to the DB
 * @author Arthur Khusnutdinov
 */
class SphinxTimerForFilesChecking extends TimerTask {

    private UnZip UnZip;

    @Override
    public void run() {
        Vars.server_thread_status = false;
        System.out.println("Transferring data via FTP suspended.");

        String driver = "com.mysql.jdbc.Driver";
        String connection = "jdbc:mysql://" + mysqlHost + ":"
                + mysqlPort + "/" + mysqlDb;

        try {
            Class.forName(driver);
            Connection con = DriverManager.getConnection(connection,
                    mysqlUser, mysqlPassword);
            Vars.stmtForMySQL = con.createStatement();

            CommonFunctions localCommonFunctions = new CommonFunctions(
                    new File(Mater_Lector));
            localCommonFunctions.indexDocs_main();

            while (current_run_indexes > 0) {
                synchronized (this) {
                    wait(5000);
                }
            }

            Vars.stmtForMySQL.close();
            con.close();
            Vars.stmtForMySQL = null;
            con = null;
            UnZip = null;
            //System.gc();

            Vars.server_thread_status = true;
            System.out.println("Transferring data via FTP continues.");

            System.out.println("Indexing done.");

        } catch (Exception ex) {
            Vars.logger.fatal("Error: ", ex);
        }
    }
}
