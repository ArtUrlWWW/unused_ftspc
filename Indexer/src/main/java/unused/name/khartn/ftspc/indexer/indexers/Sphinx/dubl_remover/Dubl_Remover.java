package name.khartn.ftspc.indexer.indexers.Sphinx.dubl_remover;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimerTask;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;

/**
 * Class for removing dublicates from the database
 * @author Arthur Khusnutdinov
 */
public class Dubl_Remover extends TimerTask {

    @Override
    public void run() {
        String pattern = "HH";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        if (format.format(new Date()).equals(Vars.dubl_remover_time) && Vars.dubl_remover_state < 1) {
            Vars.dubl_remover_state = 1;

            Statement stmt = null;
            String driver = "com.mysql.jdbc.Driver";
            String connection = "jdbc:mysql://" + Vars.mysqlHost + ":" + Vars.mysqlPort + "/" + Vars.mysqlDb;

            try {
                Class.forName(driver);
                Connection con = DriverManager.getConnection(connection, Vars.mysqlUser, Vars.mysqlPassword);
                stmt = con.createStatement();

                stmt.executeUpdate("delete from homogenus_tbl "
                        + "where id in ("
                        + "select id from ("
                        + "select id "
                        + "from homogenus_tbl "
                        + "group by text "
                        + "having (count(text)>1) "
                        + ") as x"
                        + ")");

                stmt.close();
                con.close();
                con=null;
                stmt=null;
                connection=null;
                driver=null;
                format=null;
                pattern=null;

            } catch (Exception ex) {                
                Vars.logger.fatal("Error: ", ex);
            }
            System.out.println("Removing duplicates is completed.");

            Vars.dubl_remover_state = 0;
        }
    }
}
