package name.khartn.ftspc.indexer.parsers.Archives.ZIP;

import java.io.File;
import java.io.IOException;
import java.util.zip.*;

/**
 *
 * @author Arthur Khusnutdinov
 * Class for the ZIP files checking
 */
class ZipFileCheck {

    /**
     * Method for file checking
     * @param file Path to file for checking
     * @return true if file valid, or false if the file is corrupted
     */
    boolean isValid(final File file) {
        ZipFile zipFile = null;
        try {
            zipFile = new ZipFile(file);
            return true;
        } catch (ZipException e) {
            return false;
        } catch (IOException e) {
            return false;
        } finally {
            try {
                if (zipFile != null) {
                    zipFile.close();
                    zipFile = null;
                }
            } catch (IOException e) {
            }
        }
    }
}
