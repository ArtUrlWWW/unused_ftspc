package name.khartn.ftspc.indexer.parsers.PlainText;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import name.khartn.ftspc.indexer.indexers.AddDataToIndex;
import name.khartn.ftspc.indexer.indexers.CommonFunctions;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import name.khartn.ftspc.indexer.parsers.Parser;

/**
 *
 * Class for the plain text parser
 * @author Arthur Khusnutdinov
 */
public class TextParser extends CommonFunctions implements Parser {

    private String pathToFile;
    private String fileName;

    @Override
    public void run() {
        try {
            String fileContent = "";
            Integer read;

            String fileEnc = this.detectEncoding(pathToFile);

            File fileForParsing = new File(pathToFile);
            int length = (int) fileForParsing.length();

            if (length != 0) {
                char[] cbuf = new char[length];
                InputStreamReader isr = new InputStreamReader(
                        new FileInputStream(fileForParsing), fileEnc);
                read = isr.read(cbuf);
                fileContent = new String(cbuf, 0, read);
                isr.close();

                if (!fileEnc.equals("UTF-8")) {
                    AddDataToIndex AddDataToIndex = new AddDataToIndex(null);
                    AddDataToIndex.doAddData((new String(fileContent.getBytes("UTF-8"), "UTF-8")),
                            pathToFile, fileName);
                } else {
                    AddDataToIndex AddDataToIndex = new AddDataToIndex(null);
                    AddDataToIndex.doAddData(fileContent, pathToFile, fileName);
                }
                isr = null;
                fileContent = null;
                read = null;
                cbuf = null;
                length = 0;
                fileForParsing = null;
            }
            Vars.current_run_indexes--;

        } catch (Exception ex) {
            Vars.current_run_indexes--;
            Vars.logger.fatal("Error: ", ex);
        }
    }

    @Override
    public void start_th(String pathToFile, String fileName) {
        this.pathToFile = pathToFile;
        this.fileName = fileName;
        this.start();
    }
}
