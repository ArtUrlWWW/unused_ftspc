package name.khartn.ftspc.indexer.parsers.Archives.ZIP;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.Enumeration;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class UnZip {

    /**
     * The main method in the class used to process the input files.
     * @param filename The name of the file for processing.
     */
    public void doUnZip(String filename) {
        try {
            String fileName = "";
            String destinationFolder = "";
            String consoleEnc = System.getProperty("console.encoding", "Cp866");
            BufferedInputStream is;
            BufferedOutputStream dest;
            FileOutputStream fos;
            File nameForDirCreating;
            File destination = null;
            ZipFile zipfile;
            ZipEntry entry;
            ZipFile zipfile_test = new ZipFile(filename, consoleEnc);
            Enumeration e;
            InputStream entryStream;
            Boolean folderCreated = false;
            int BUFFER = 2048;
            int count;
            byte data[];

            // For first, check, what folder doesn't exist
            while (!folderCreated) {
                destinationFolder = Vars.Mater_Lector + "/"
                        + (new Date()).getTime() + "/";
                destination = new File(destinationFolder);
                if (destination.mkdirs()) {
                    folderCreated = true;
                }
            }

            if (zipfile_test.getEncoding() == null) {
                zipfile = new ZipFile(filename, consoleEnc);
            } else {
                zipfile = new ZipFile(filename, zipfile_test.getEncoding());
            }
            zipfile_test.close();

            e = zipfile.getEntries();
            while (e.hasMoreElements()) {
                entry = (ZipEntry) e.nextElement();
                fileName = entry.getName();
                if (fileName.endsWith("/")) {
                    nameForDirCreating = new File(destinationFolder + entry.getName());
                    nameForDirCreating.mkdirs();
                }
            }

            e = zipfile.getEntries();
            while (e.hasMoreElements()) {
                count = 0;
                data = new byte[BUFFER];
                entry = (ZipEntry) e.nextElement();

                fileName = destinationFolder + entry.getName();
                if (!fileName.endsWith("/")) {
                    //System.out.println("Extracting " + fileName);
                    entryStream = zipfile.getInputStream(entry);
                    is = new BufferedInputStream(entryStream);
                    fos = new FileOutputStream(fileName);
                    dest = new BufferedOutputStream(fos, BUFFER);

                    while ((count = is.read(data, 0, BUFFER)) != -1) {
                        dest.write(data, 0, count);
                    }

                    dest.flush();
                    dest.close();
                    is.close();
                    entryStream.close();
                }
            }
            zipfile.close();

            fileName = null;
            destinationFolder = null;
            destination = null;
            consoleEnc = null;
            is = null;
            dest = null;
            fos = null;
            zipfile = null;
            entry = null;
            zipfile_test = null;
            e = null;
            BUFFER = 0;
            count = 0;
            data = null;
        } catch (Exception ex) {
            Vars.logger.fatal("Error: ", ex);
        }
    }
}
