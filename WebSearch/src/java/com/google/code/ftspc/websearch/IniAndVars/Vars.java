package com.google.code.ftspc.websearch.IniAndVars;

import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.lucene.search.IndexSearcher;

/**
 * Class with the static (global) variables
 * @author Arthur Khusnutdinov
 */
public class Vars {

    /**
     * The path to the index Lucene.
     */
    public static String mainConfigFilePath = "c:/MyProjects/FTSPC/trunk/System/main_config.ini";
    public static String Lucene_Repo = "";
    public static Logger logger = null;
    public static String rootPathOfApplication = "";
    public static Properties velocityProps = null;
    public static IndexSearcher searcher;
}
