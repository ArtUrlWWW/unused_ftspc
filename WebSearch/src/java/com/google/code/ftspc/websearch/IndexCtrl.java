package com.google.code.ftspc.websearch;

import com.google.code.ftspc.websearch.Lucene.SearchFunctions;
import com.google.code.ftspc.websearch.velocity.TemplateMaster;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class IndexCtrl {

    PrintWriter out;
    HttpServletRequest request;

    public IndexCtrl(PrintWriter out, HttpServletRequest request) {
        this.out = out;
        this.request = request;
    }

    public void proceed() {
        TemplateMaster TemplateMaster = new TemplateMaster();
        SearchFunctions searchFunctions = new SearchFunctions();
        String searchPhrase;

        TemplateMaster.initialize("SearchMain.html");

        searchPhrase = request.getParameter("searchPhrase");
        if (searchPhrase != null) {
            TemplateMaster.context.put("firmTypesList",
                    searchFunctions.search(searchPhrase));
        }
        TemplateMaster.proceedSearchPage(out);

    }
}
