package com.google.code.ftspc.websearch.IniAndVars;

import com.google.code.ftspc.websearch.velocity.TemplateMaster;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Properties;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class WebSearchIni {

    public static void loadProps() {
        try {
            if (Vars.velocityProps == null) {
                InputStream propertiesIn =
                        TemplateMaster.class.getResourceAsStream("velocity.properties");
                if (propertiesIn == null) {
                    throw new FileNotFoundException("${archive}/com.google.code.ftspc.websearch.velocity/velocity.properties");
                }
                Properties properties = new Properties();
                properties.load(propertiesIn);
                properties.setProperty("file.resource.loader.path",
                        Vars.rootPathOfApplication + "WEB-INF/classes/com/google/code/ftspc/websearch/velocity/");
                Vars.velocityProps = properties;

                if ((new File("../../main_config.ini")).exists()) {
                    properties = new Properties();
                    propertiesIn = new FileInputStream("../../main_config.ini");
                    properties.load(propertiesIn);
                } else {
                    properties = new Properties();
                    propertiesIn = new FileInputStream(Vars.mainConfigFilePath);
                    properties.load(propertiesIn);
                }

                Vars.Lucene_Repo = properties.getProperty("Lucene_Repo");
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public String convertStreamToString(InputStream is)
            throws IOException {
        /*
         * To convert the InputStream to String we use the
         * Reader.read(char[] buffer) method. We iterate until the
         * Reader return -1 which means there's no more data to
         * read. We use the StringWriter class to produce the string.
         */
        if (is != null) {
            Writer writer = new StringWriter();

            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(
                        new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        } else {
            return "";
        }
    }
}
