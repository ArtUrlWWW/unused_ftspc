package com.google.code.ftspc.websearch.velocity;

import com.google.code.ftspc.websearch.IniAndVars.Vars;
import com.google.code.ftspc.websearch.IniAndVars.WebSearchIni;
import java.io.PrintWriter;
import java.io.StringWriter;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class TemplateMaster {

    public VelocityContext context;
    VelocityEngine engine;
    StringWriter writer;
    Template pageTemplate;

    public void initialize(String template) {
        context = new VelocityContext();
        engine = new VelocityEngine();
        writer = new StringWriter();
        WebSearchIni.loadProps();
        try {
            engine.init(Vars.velocityProps);
            pageTemplate = engine.getTemplate(template, "UTF-8");

        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }

    public void proceedSearchPage(PrintWriter out) {
        try {
            pageTemplate.merge(context, writer);
            out.println(writer.toString());
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }
}
