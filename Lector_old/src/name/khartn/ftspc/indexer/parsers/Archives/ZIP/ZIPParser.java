package name.khartn.ftspc.indexer.parsers.Archives.ZIP;

import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import name.khartn.ftspc.indexer.parsers.Parser;
import java.io.File;

/**
 * Class for the DOC parser
 * @author Arthur Khusnutdinov
 */
public class ZIPParser extends Thread implements Parser {

    private String pathToFile;

    @Override
    public void run() {
        File fileForUnpacking = new File(pathToFile);
        ZipFileCheck ZipFileCheck = new ZipFileCheck();
        UnZip UnZip = new UnZip();
        try {
            if (ZipFileCheck.isValid(fileForUnpacking)) {
                UnZip.doUnZip(pathToFile);
            }

            fileForUnpacking.delete();
            fileForUnpacking = null;
            Vars.current_run_indexes--;

        } catch (Exception ex) {
            Vars.current_run_indexes--;
            Vars.logger.fatal(ex);
        }
    }

    @Override
    public void start_th(String pathToFile, String fileName) {
        this.pathToFile = pathToFile;
        this.start();
    }
}
