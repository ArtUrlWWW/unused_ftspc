package name.khartn.ftspc.indexer.parsers;

/**
 * Class for the interface for the parsers
 * @author Arthur Khusnutdinov
 */
public interface Parser {

    /**
     * Interface for the parser
     * @param pathToFile pathToFile Path to the file to parse
     * @param fileName File name 
     */
    public void start_th(String pathToFile, String fileName);
}
