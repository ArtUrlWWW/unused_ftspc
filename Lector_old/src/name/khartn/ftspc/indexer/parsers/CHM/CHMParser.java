package name.khartn.ftspc.indexer.parsers.CHM;

import name.khartn.ftspc.indexer.indexers.AddDataToIndex;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import name.khartn.ftspc.indexer.parsers.Parser;
import com.google.code.jchmweb.jchmweb2.Configuration.ParamsClass;
import java.io.File;
import java.util.Date;

/**
 * This class now for testing only.
 * Class for the HTML parser
 * @author Arthur Khusnutdinov
 */
public class CHMParser extends Thread implements Parser {

    private String pathToFile;
    private String fileName;

    @Override
    public void run() {
        try {
            ParamsClass.logger = Vars.logger;
            String fileContent = "";
            ChmExtract localChmExtract = new ChmExtract();
            String tmpDir = Vars.PathToTmpDir + "/" + (new Date()).getTime();
            File tmpDirForDeleting = new File(tmpDir);
            CHMParserFunctions CHMParserFunctions = new CHMParserFunctions();
            localChmExtract.main(pathToFile, tmpDir);
            AddDataToIndex AddDataToIndex = new AddDataToIndex(null);

            fileContent = CHMParserFunctions.processUnpackedObjects(tmpDirForDeleting);
            CHMParserFunctions.deleteDirectory(tmpDirForDeleting);
            AddDataToIndex.doAddData(fileContent, pathToFile, fileName);
            Vars.current_run_indexes--;

        } catch (Exception ex) {
            Vars.current_run_indexes--;
            Vars.logger.fatal("Error: ", ex);
        }
    }

    @Override
    public void start_th(String pathToFile, String fileName) {
        this.pathToFile = pathToFile;
        this.fileName = fileName;
        this.start();
    }
}
