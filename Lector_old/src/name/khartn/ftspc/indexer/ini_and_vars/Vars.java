package name.khartn.ftspc.indexer.ini_and_vars;

import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.ftpserver.FtpServer;
import org.apache.log4j.Logger;
import org.apache.lucene.index.IndexWriter;
import org.apache.tika.Tika;
import org.knallgrau.utils.textcat.TextCategorizer;
import org.quartz.Scheduler;

/**
 * Class with the static (global) variables
 * @author Arthur Khusnutdinov
 */
public class Vars {

    /**
     * Number of index threads
     */
    public static Integer current_run_indexes = 0;
    /**
     * Maximal number of index threads
     */
    public static Integer max_threads = 3;
    /**
     * OS Type: 0-unknown, 1-x32 Windows, 2-x64 Windows (amd64)
     */
    public static Integer os = 0;
/////////////////////////////////////////////////////////////////
//    MySQL Section
/////////////////////////////////////////////////////////////////
    /**
     * MySQL host
     */
    public static String mysql_host = "";
    /**
     * MySQL port
     */
    public static String mysql_port = "";
    /**
     * MySQL database for use
     */
    public static String mysql_db = "";
    /**
     * MySQL user name
     */
    public static String mysql_user = "";
    /**
     * MySQL user's password
     */
    public static String mysql_password = "";
/////////////////////////////////////////////////////////////////
//    Lector settings
/////////////////////////////////////////////////////////////////
    /**
     * Folder where the we will upload files as Lector from the same folder will handle the files.
     */
    public static String Mater_Lector = "";
    /**
     * Folder where Lector will move the processed files.
     */
    public static String Lector_Repository = "";
    /**
     * Period of time when the Lector will scan the files in FTP_Mater_Lector.
     */
    public static String CronExpressionForIndexer = "";
    /**
     * Status RemDublicate: 0 - not running, 0 - running.
     */
    public static Integer dubl_remover_state = 0;
    /**
     * Date when RemDublicate should run.
     */
    public static String dubl_remover_time = "";
    /**
     * Type indexer - Lucene or Sphinx.
     */
    public static String IndexerToUse = "";
    /**
     * The path to the index Lucene.
     */
    public static String Lucene_Repo = "";
    /**
     * Variable to store a list of parsers obtained from an external XML file.
     */
    public static Map<String, Map> parsersFromXML = new HashMap<String, Map>();
    /**
     * Variable to store Writer Lucene
     */
    public static IndexWriter writerForLucene = null;
    /**
     * Variable to store the MySQL Statement for the index Sphinx
     */
    public static Statement stmtForMySQL = null;
    /**
     * Logger object.
     */
    public static Logger logger = null;
    /**
     * Recognizer of language.
     */
    public static TextCategorizer TextCategorizerLocal = new TextCategorizer();
    /**
     * Object Tika.
     */
    public static Tika tika = new Tika();
    /**
     * An object of the scheduler.
     */
    public static Scheduler sched = null;
    /**
     * The path to the directory for temporary files.
     */
    public static String PathToTmpDir;
    /**
     * Flag to determine whether running the indexer or not.
     */
    public static Boolean isIndexerRunning = false;
/////////////////////////////////////////////////////////////////
//    FTP settings
/////////////////////////////////////////////////////////////////
    /**
     * Thread of the FTP process.
     */
    public static Thread server_thread;
    /**
     * Status of the FTP server.
     * //true - running, false - stopped
     */
    public static boolean server_thread_status = false;
    /**
     * Total size of the processed files
     */
    public static Double totalSizeOfProcessedFiles = 0.0;
    /**
     * Indexer's execution start time
     */
    public static Date startTime;
    /**
     * Indexer's execution end time
     */
    public static Date endTime;
    /**
     * Time spent on the iteration index.
     */
    public static Long elapsed_time;
    /**
     * File separator ('/' or '\')
     */
    public static String fileSeparator="";
    /**
     * The path to the folder in which to move indexed files.
     */
    public static String pathToDayDir="";
    /**
     * FTP Server object.
     */
    public static FtpServer server;
}
