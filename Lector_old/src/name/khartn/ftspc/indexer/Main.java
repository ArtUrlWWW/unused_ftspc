package name.khartn.ftspc.indexer;

import java.util.Timer;
import name.khartn.ftspc.indexer.Server.FTP.FTPThread;
import name.khartn.ftspc.indexer.indexers.Indexer;
import name.khartn.ftspc.indexer.ini_and_vars.Lector_Ini;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import static org.quartz.JobBuilder.newJob;
import org.quartz.JobDetail;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import org.quartz.Trigger;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Main Class
 * @author Arthur Khusnutdinov
 */
public class Main {
//-Xmx1700m -Xms1500m

    /**
     * Main function
     * @param args Not used.
     */
    public static void main(String[] args) {
        Vars.fileSeparator = System.getProperty("file.separator");
        Indexer indexer;
        Vars.server_thread_status = true;
        try {
            Lector_Ini.class.newInstance().configure();

            FTPThread FTPThreadLocal = new FTPThread();
            FTPThreadLocal.start();

            indexer = (Indexer) (Class.forName(Vars.IndexerToUse)).newInstance();
            indexer.startIndex();

//            Timer dubl_remover_Timer = new Timer();
//            dubl_remover_Timer.schedule(new Dubl_Remover(), 0, 3600000);
            Timer InfoThreadTimer = new Timer();
            InfoThreadTimer.schedule(new InfoThread(), 0, 1000);


            JobDetail job = newJob(name.khartn.ftspc.indexer.CheckDateForPath.class).
                    withIdentity("jobForCheckDateForPath", "Lector").build();

            Trigger trigger = newTrigger().withIdentity("triggerForCheckDateForPath", "Lector").
                    withSchedule(simpleSchedule().withIntervalInHours(1).
                    repeatForever()).startNow().build();

            Vars.sched.scheduleJob(job, trigger);
        } catch (Exception ex) {
            ex.printStackTrace();
            Vars.logger.fatal("Error: ", ex);
            ex.printStackTrace();
        }
    }
}
