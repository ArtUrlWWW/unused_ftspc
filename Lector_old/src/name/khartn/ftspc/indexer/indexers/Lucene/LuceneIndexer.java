package name.khartn.ftspc.indexer.indexers.Lucene;

import java.util.Date;
import org.quartz.CronTrigger;
import name.khartn.ftspc.indexer.indexers.Indexer;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import org.quartz.JobDetail;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Lucene's indexer class
 * @author Arthur Khusnutdinov
 */
public class LuceneIndexer implements Indexer {

    @Override
    public void startIndex() {
        try {
            JobDetail job = newJob(name.khartn.ftspc.indexer.indexers.Lucene.LuceneTimerForFilesChecking.class).
                    withIdentity("jobForIndexer", "Lector").build();
            CronTrigger trigger = newTrigger().
                    withIdentity("triggerForIndexer", "Lector").
                    withSchedule(cronSchedule(Vars.CronExpressionForIndexer)).build();

            Date ft = Vars.sched.scheduleJob(job, trigger);
            System.out.println(job.getKey() + " has been scheduled to run at: " + ft
                    + " and repeat based on expression: "
                    + trigger.getCronExpression());
        } catch (Exception ex) {
            Vars.logger.warn(ex.getMessage(), ex);
        }
    }
}
