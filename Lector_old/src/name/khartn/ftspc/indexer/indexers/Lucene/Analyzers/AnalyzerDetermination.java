package name.khartn.ftspc.indexer.indexers.Lucene.Analyzers;

import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.util.Version;

/**
 * Class for the language determination
 * @author Arthur Khusnutdinov
 */
public class AnalyzerDetermination {

    /**
     * The method for selecting Analyzer for appropriate language.
     * @param language A string containing the name of the language.
     * @return Returns the type of Analyzer.
     */
    public PerFieldAnalyzerWrapper getAnalyzer(String language) {
        if (language.equals("russian")) {
            return (new PerFieldAnalyzerWrapper(new RussianAnalyzer(Version.LUCENE_40)));
        }
        if (language.equals("english")) {
            return (new PerFieldAnalyzerWrapper(new StandardAnalyzer(Version.LUCENE_40)));
        }

        return (new PerFieldAnalyzerWrapper(new StandardAnalyzer(Version.LUCENE_40)));
    }
}
