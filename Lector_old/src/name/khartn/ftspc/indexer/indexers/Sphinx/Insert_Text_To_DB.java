package name.khartn.ftspc.indexer.indexers.Sphinx;

import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import java.sql.Statement;

/**
 * Class for the inserting texts from files to DB's table.
 * @author Arthur Khusnutdinov
 */
public class Insert_Text_To_DB extends Thread {

    private Statement stmt;
    private String sql_to_do;
    private String file_name;

    /**
     * Constructor for the thread starting
     * @param stmt Statement for the operations with the data in DB.
     * @param sql_to_do String with the SQL commands
     * @param file_name Path to file
     */
    public void start_th(Statement stmt, String sql_to_do, String file_name) {
        this.stmt = stmt;
        this.sql_to_do = sql_to_do;
        this.file_name = file_name;
        this.start();
    }

    @Override
    public void run() {

        if (execute_insert()) {
        }
    }

    /**
     * Method for storing data in the database.
     * @return Returns True, if the insertion into the database
     * was successful and False, if you can not insert data into the database.
     */
    private Boolean execute_insert() {
        try {
            stmt.execute(sql_to_do);           
            return true;
        } catch (Exception ex) {
            System.out.println("Error: " + sql_to_do + " "
                    + Vars.current_run_indexes);
            Vars.logger.fatal("Error: ", ex);
            return false;
        }
    }
}
