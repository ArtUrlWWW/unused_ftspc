package name.khartn.ftspc.indexer.indexers;

/**
 * Interface for indexers
 * @author Arthur Khusnutdinov
 */
public interface Indexer {

    /**
     * A method interface to start indexing.
     */
    public void startIndex();
}
