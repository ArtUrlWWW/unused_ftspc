package name.khartn.ftspc.indexer;

import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import java.io.File;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class CheckDateForPath implements Job {

    /**
     * The method of the timer.
     * @param context
     * @throws JobExecutionException
     */
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        Date date = new Date();
        Format yearFormat = new SimpleDateFormat("yyyy");
        Format monthFormat = new SimpleDateFormat("MM");
        Format dayFormat = new SimpleDateFormat("dd");
        String pathToDayDir = Vars.Lector_Repository + "/" + yearFormat.format(date)
                + "/" + monthFormat.format(date) + "/" + dayFormat.format(date);
        File pathToDayDirFile = new File(pathToDayDir);
        if (!pathToDayDirFile.exists()) {
            pathToDayDirFile.mkdirs();
        }
        Vars.pathToDayDir = pathToDayDir;
    }
}
