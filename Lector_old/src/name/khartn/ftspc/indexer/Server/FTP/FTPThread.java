package name.khartn.ftspc.indexer.Server.FTP;

import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.Authority;
import org.apache.ftpserver.ftplet.UserManager;
import org.apache.ftpserver.usermanager.PropertiesUserManagerFactory;
import org.apache.ftpserver.usermanager.SaltedPasswordEncryptor;
import org.apache.ftpserver.usermanager.impl.BaseUser;
import org.apache.ftpserver.usermanager.impl.WritePermission;

/**
 * Class for FTP server initialization.
 * @author Arthur Khusnutdinov
 */
public class FTPThread extends Thread {

    @Override
    public void run() {
        File myUsersProperties = new File("../myusers.properties");
        Boolean createNewUsers = false;

        try {
            PropertiesUserManagerFactory userManagerFactory = new PropertiesUserManagerFactory();
            if (!myUsersProperties.exists()) {
                myUsersProperties.createNewFile();
                createNewUsers = true;
            }
            userManagerFactory.setFile(myUsersProperties);
            userManagerFactory.setPasswordEncryptor(new SaltedPasswordEncryptor());
            UserManager um = userManagerFactory.createUserManager();

            if (createNewUsers) {
                List<Authority> auths = new ArrayList<Authority>();
                Authority auth = new WritePermission();
                auths.add(auth);

                BaseUser user = new BaseUser();
                user.setName("myNewUser");
                user.setPassword("secret");
                user.setHomeDirectory(Vars.Mater_Lector);
                user.setAuthorities(auths);


                um.save(user);
            }

            FtpServerFactory serverFactory = new FtpServerFactory();
            Vars.server = serverFactory.createServer();

            serverFactory.setUserManager(um);

            // start the server
            Vars.server.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
