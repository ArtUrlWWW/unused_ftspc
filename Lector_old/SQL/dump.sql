CREATE DATABASE `sphinx` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `sphinx`;

DROP TABLE IF EXISTS `homogenus_tbl`;
CREATE TABLE IF NOT EXISTS `homogenus_tbl` (
  `id` int(11) NOT NULL auto_increment,
  `text` longtext NOT NULL,
  `filename` varchar(777) NOT NULL,
  `filename_orig` varchar(777) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ;

CREATE USER 'test'@'localhost';
SET PASSWORD FOR 'test'@'localhost' = PASSWORD('test');
GRANT ALL ON sphinx.* TO 'test'@'localhost';