package com.google.code.ftspc.LectorInstaller.SomeFunctions;

import com.google.code.ftspc.LectorInstaller.LectorInstallerApp;
import com.google.code.ftspc.LectorInstaller.MainFrame;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author AKhusnutdinov
 */
public class SomeFunctionsForInstaller {

    public boolean deleteDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }

    public void CreateShortcuts(String destinationFolder, File tmpDir,
            ArrayList<Map> fileNamesForShortcuts) {
        FileOutputStream fo = null;
        try {
            StringBuilder script = new StringBuilder();

            String localDestinationFolder = "";
            MainFrame.jLabel7.setText("Creating shortcuts...");
            localDestinationFolder = destinationFolder.replace("/", "\\");
            localDestinationFolder = localDestinationFolder.replace("\\\\", "\\");
            script.append("Set sh = CreateObject(\"WScript.Shell\")"
                    + " \nset WshShell = WScript.CreateObject(\"WScript.Shell\") "
                    + " \n strDesktop = WshShell.SpecialFolders(\"Desktop\") "
                    + "\n Const ForReading = 1, ForWriting = 2, ForAppending = 8"
                    + "\n   Dim fso, f"
                    + "\n   Set fso = CreateObject(\"Scripting.FileSystemObject\")"
                    + "\n    Set f = fso.OpenTextFile(\""
                    + localDestinationFolder
                    + "shortcuts.win.txt\", ForAppending, True)");
            for (int x = 0; x < fileNamesForShortcuts.size(); x++) {
                script.append("\nSet shortcut = sh.CreateShortcut(strDesktop & \"\\"
                        + fileNamesForShortcuts.get(x).get(0) + "\")"
                        + "\nshortcut.TargetPath = \""
                        + fileNamesForShortcuts.get(x).get(1) + "\""
                        + "\nshortcut.WorkingDirectory = \""
                        + localDestinationFolder.substring(0, localDestinationFolder.length() - 1) + "\""
                        + "\nshortcut.Save"
                        + "\n   f.WriteLine shortcut");
            }
            script.append("\n   f.Close");


            File file = new File(tmpDir.getAbsolutePath() + "/temp.vbs");
            fo = new FileOutputStream(file);
            fo.write(script.toString().getBytes());
            fo.close();
            Runtime.getRuntime().exec("wscript.exe " + file.getAbsolutePath());
            synchronized (this) {
                wait(15000);
            }
        } catch (Exception ex) {
            LectorInstallerApp.logger.fatal(ex.getMessage(), ex);
        } finally {
            try {
                fo.close();
            } catch (IOException ex) {
                LectorInstallerApp.logger.fatal(ex.getMessage(), ex);
            }
        }
    }

    public void prepareIniFile(String destinationFolder) {
        StringBuilder text = new StringBuilder();
        String NL = System.getProperty("line.separator");
        String stringTmpLine = "";
        Scanner scanner = null;
        Writer out = null;
        try {
            scanner = new Scanner(new FileInputStream(destinationFolder + "main_config.ini"), "UTF-8");
            while (scanner.hasNextLine()) {
                stringTmpLine = scanner.nextLine();
                if (stringTmpLine.contains("Mater_Lector")) {
                    stringTmpLine = "Mater_Lector=" + MainFrame.MaterLectorPath.getText() + "/";
                }
                if (stringTmpLine.contains("Lector_Repository")) {
                    stringTmpLine = "Lector_Repository=" + MainFrame.LectorRepositoryPath.getText() + "/";
                }
                if (stringTmpLine.contains("Lucene_Repo")) {
                    stringTmpLine = "Lucene_Repo=" + MainFrame.LuceneRepoPath.getText() + "/";
                }
                text.append(stringTmpLine).append(NL);
            }
            out = new OutputStreamWriter(
                    new FileOutputStream(destinationFolder + "main_config.ini"), "UTF-8");
            out.write(text.toString().replace("\\", "/"));
        } catch (Exception ex) {
            LectorInstallerApp.logger.fatal(ex.getMessage(), ex);
        } finally {
            try {
                scanner.close();
                out.close();
            } catch (Exception ex) {
                LectorInstallerApp.logger.fatal(ex.getMessage(), ex);
            }
        }
    }

    public void ExtractUnInstaller(String destinationFolder) {
        OutputStream out = null;
        MainFrame.jLabel7.setText("Unpacking UnInstaller.");
        try {
            File f = new File(destinationFolder + "/LectorUnInstallerAllInOne.jar");
            InputStream inputStream =
                    MainFrame.class.getResourceAsStream(
                    "LectorUnInstallerAllInOne.jar");
            out = new FileOutputStream(f);
            byte[] buf = new byte[1024];
            int len;
            while ((len = inputStream.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            inputStream.close();

        } catch (Exception ex) {
            LectorInstallerApp.logger.fatal(ex.getMessage(), ex);
        }
    }
}
