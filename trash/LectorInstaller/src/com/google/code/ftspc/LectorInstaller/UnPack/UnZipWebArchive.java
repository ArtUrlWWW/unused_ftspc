package com.google.code.ftspc.LectorInstaller.UnPack;

import com.google.code.ftspc.LectorInstaller.LectorInstallerApp;
import com.google.code.ftspc.LectorInstaller.MainFrame;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class UnZipWebArchive {

    public void ExtractWebArchive() {
        File forDirList = new File(MainFrame.destinationFolder);
        String filesList[] = forDirList.list();
        for (String fileName : filesList) {
            File fileForTest = new File(MainFrame.destinationFolder + fileName);
            if (fileForTest.isDirectory()) {
                if (fileName.toLowerCase().indexOf("tomcat") > -1) {
                    extract(MainFrame.destinationFolder + fileName);
                    break;
                }
            }
        }

    }

    private void extract(String destinationFolder) {
        OutputStream out = null;
        MainFrame.jLabel7.setText("Unpacking UnInstaller.");
        try {
            File f = new File(destinationFolder + "/webapps/WebSearch.war");
            InputStream inputStream =
                    MainFrame.class.getResourceAsStream(
                    "WebSearch.war");
            out = new FileOutputStream(f);
            byte[] buf = new byte[1024];
            int len;
            while ((len = inputStream.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            inputStream.close();

        } catch (Exception ex) {
            LectorInstallerApp.logger.fatal(ex.getMessage(), ex);
        }
    }
}
