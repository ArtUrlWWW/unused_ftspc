package com.google.code.ftspc.LectorInstaller.UnPack;

import com.google.code.ftspc.LectorInstaller.LectorInstallerApp;
import com.google.code.ftspc.LectorInstaller.MainFrame;
import com.google.code.ftspc.LectorInstaller.SomeFunctions.SomeFunctionsForInstaller;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;
import org.w3c.dom.*;
import org.apache.xerces.dom.DocumentImpl;
import org.apache.xml.serialize.*;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class UnZip extends Thread {

    private String filename;
    private File tmpDir;
    public static Boolean UnZipDone = false;

    public UnZip(File tmpDir) {
        MainFrame.destinationFolder =
                (MainFrame.InstallationDirectory.getText() + "/").replace("\\", "/");
        this.tmpDir = tmpDir;
        this.filename = tmpDir.getAbsolutePath() + "/bins.zip";
    }

    /**
     * The main method in the class used to process the input files.
     */
    @Override
    public void run() {
        try {
            String localFileName = "";
            String consoleEnc = System.getProperty("console.encoding", "Cp866");
            BufferedInputStream is;
            BufferedOutputStream dest;
            FileOutputStream fos;
            File nameForDirCreating;
            ZipFile zipfile;
            ZipEntry entry;
            ZipFile zipfile_test = new ZipFile(filename, consoleEnc);
            Enumeration e;
            InputStream entryStream;
            int BUFFER = 2048;
            int count;
            float countFiles = 0;
            float countProcessedFiles = 0;
            int countProgress;
            byte data[];
            FileOutputStream fosForXML;
            OutputFormat of;
            XMLSerializer serializer;
            Element XMLelement = null;
            Node XMLnode = null;
            // Document (Xerces implementation only).
            Document xmldoc = new DocumentImpl();
            // Root element.
            Element mainElement = xmldoc.createElement("mainElement");
            Element rootFilesForDeleting = xmldoc.createElement("filesForDeleting");
            Element rootDirsForDeleting = xmldoc.createElement("foldersForDeleting");
            Element mainDir = xmldoc.createElement("mainDir");
            SomeFunctionsForInstaller SomeFunctionsForInstaller = new SomeFunctionsForInstaller();
            Map<Integer, String> tmpfileNamesForShortcuts = new HashMap();
            ArrayList<Map> fileNamesForShortcuts = new ArrayList<Map>();
            SomeFunctionsForInstaller SomeFunctionsForInstallerLocal=new SomeFunctionsForInstaller();

            if (zipfile_test.getEncoding() == null) {
                zipfile = new ZipFile(filename, consoleEnc);
            } else {
                zipfile = new ZipFile(filename, zipfile_test.getEncoding());
            }
            zipfile_test.close();

            e = zipfile.getEntries();
            while (e.hasMoreElements()) {
                countFiles++;
                entry = (ZipEntry) e.nextElement();
                localFileName = entry.getName();
                if (localFileName.endsWith("/")) {
                    localFileName = MainFrame.destinationFolder + localFileName;
                    XMLelement = xmldoc.createElementNS(null, "folderForDeleting");
                    XMLelement.setAttributeNS(null, "pathToDir", localFileName);
                    XMLnode = xmldoc.createTextNode(localFileName);
                    XMLelement.appendChild(XMLnode);
                    rootDirsForDeleting.appendChild(XMLelement);

                    nameForDirCreating = new File(MainFrame.destinationFolder + entry.getName());
                    nameForDirCreating.mkdirs();
                }
            }

            e = zipfile.getEntries();
            while (e.hasMoreElements()) {
                countProcessedFiles++;
                countProgress = (int) (countProcessedFiles / countFiles * 100);
                SwingUtilities.invokeLater(new UpdateProgressBarTask(
                        MainFrame.jPB1, countProgress));

                MainFrame.jPB1.repaint();
                count = 0;
                data = new byte[BUFFER];
                entry = (ZipEntry) e.nextElement();

                localFileName = MainFrame.destinationFolder + entry.getName();
                if (!localFileName.endsWith("/")) {
                    XMLelement = xmldoc.createElementNS(null, "fileForDeleting");
                    XMLelement.setAttributeNS(null, "pathToFile", localFileName);
                    XMLnode = xmldoc.createTextNode(localFileName);
                    XMLelement.appendChild(XMLnode);
                    rootFilesForDeleting.appendChild(XMLelement);

                    //System.out.println("Extracting " + localFileName);

                    entryStream = zipfile.getInputStream(entry);

                    is = new BufferedInputStream(entryStream);
                    fos = new FileOutputStream(localFileName);
                    dest = new BufferedOutputStream(fos, BUFFER);

                    while ((count = is.read(data, 0, BUFFER)) != -1) {
                        dest.write(data, 0, count);
                    }

                    dest.flush();
                    dest.close();
                    is.close();
                    entryStream.close();
                }
            }
            zipfile.close();

            XMLelement = xmldoc.createElementNS(null, "mainDirForDeleting");
            XMLelement.setAttributeNS(null, "pathToDir", MainFrame.destinationFolder);
            XMLnode = xmldoc.createTextNode(MainFrame.destinationFolder);
            XMLelement.appendChild(XMLnode);
            mainDir.appendChild(XMLelement);

            mainElement.appendChild(rootFilesForDeleting);
            mainElement.appendChild(rootDirsForDeleting);
            mainElement.appendChild(mainDir);
            xmldoc.appendChild(mainElement);

            fosForXML = new FileOutputStream(MainFrame.destinationFolder + "fileList.xml");
            of = new OutputFormat("XML", "UTF-8", true);
            of.setIndent(1);
            of.setIndenting(true);
            serializer = new XMLSerializer(fosForXML, of);
            serializer.asDOMSerializer();
            serializer.serialize(xmldoc.getDocumentElement());
            fosForXML.close();

            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    MainFrame.jPB1.setValue(100);
                }
            });

            // Название ярлыка для папки
            tmpfileNamesForShortcuts.put(0, "FTSPC.lnk");
            tmpfileNamesForShortcuts.put(1, MainFrame.destinationFolder.replace("/", "\\"));
            fileNamesForShortcuts.add(tmpfileNamesForShortcuts);
            for (int x = 0; x < fileNamesForShortcuts.size(); x++) {
                fileNamesForShortcuts.get(x).get(0);
            }

            if (MainFrame.jCheckBox2.isSelected()) {
                if (MainFrame.jCheckBox1.isSelected()) {
                    tmpfileNamesForShortcuts = new HashMap();
                    tmpfileNamesForShortcuts.put(0, "UnInstall FTSPC.lnk");
                    tmpfileNamesForShortcuts.put(1, MainFrame.destinationFolder.replace("/", "\\") + "\\LectorUnInstallerAllInOne.jar");
                    fileNamesForShortcuts.add(tmpfileNamesForShortcuts);
                }
                SomeFunctionsForInstaller.ExtractUnInstaller(MainFrame.destinationFolder);
            }

            if (MainFrame.jCheckBox1.isSelected()) {
                SomeFunctionsForInstaller.CreateShortcuts(MainFrame.destinationFolder, tmpDir, fileNamesForShortcuts);
            }

            MainFrame.jLabel7.setText("Deleting temp files.");
            SomeFunctionsForInstaller.deleteDirectory(tmpDir);

            MainFrame.jLabel7.setText("Updating main ini file.");
            SomeFunctionsForInstaller.prepareIniFile(MainFrame.destinationFolder);

            UnZipDone = true;

        } catch (Exception ex) {
            LectorInstallerApp.logger.fatal(ex.getMessage(), ex);
        }
    }

    public void start_th() {
        this.start();
    }

    private class UpdateProgressBarTask implements Runnable {

        private JProgressBar progressBar;
        private int value;

        public UpdateProgressBarTask(JProgressBar progressBar, int value) {
            this.value = value;
            this.progressBar = progressBar;
        }

        @Override
        public void run() {
            if (this.progressBar != null) {
                this.progressBar.setValue(value);
            }
        }
    }
}