package com.google.code.ftspc.LectorInstaller.UnPack;

import com.google.code.ftspc.LectorInstaller.Downloads.DownloadApacheTomcat;
import com.google.code.ftspc.LectorInstaller.LectorInstallerApp;
import com.google.code.ftspc.LectorInstaller.MainFrame;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Enumeration;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class UnZipTomCat extends Thread {

    private String filename;
    private final String destinationFolder = MainFrame.InstallationDirectory.getText() + "/";
    public static Boolean UnZipDone = false;
    DownloadApacheTomcat downloadForm;
    MainFrame parentForm;

    public UnZipTomCat(String archiveFileName, DownloadApacheTomcat downloadForm, MainFrame parentForm) {
        this.filename = MainFrame.destinationFolder + archiveFileName;
        this.downloadForm = downloadForm;
        this.parentForm = parentForm;
    }

    /**
     * The main method in the class used to process the input files.
     */
    @Override
    public void run() {
        try {
            String localFileName = "";
            String consoleEnc = System.getProperty("console.encoding", "Cp866");
            BufferedInputStream is;
            BufferedOutputStream dest;
            FileOutputStream fos;
            File nameForDirCreating;
            ZipFile zipfile;
            ZipEntry entry;
            ZipFile zipfile_test = new ZipFile(filename, consoleEnc);
            Enumeration e;
            InputStream entryStream;
            int BUFFER = 2048;
            int count;
            float countFiles = 0;
            float countProcessedFiles = 0;
            int countProgress;
            byte data[];


            if (zipfile_test.getEncoding() == null) {
                zipfile = new ZipFile(filename, consoleEnc);
            } else {
                zipfile = new ZipFile(filename, zipfile_test.getEncoding());
            }
            zipfile_test.close();

            e = zipfile.getEntries();
            while (e.hasMoreElements()) {
                countFiles++;
                entry = (ZipEntry) e.nextElement();
                localFileName = entry.getName();
                if (localFileName.endsWith("/")) {
                    nameForDirCreating = new File(destinationFolder + entry.getName());
                    nameForDirCreating.mkdirs();
                }
            }

            e = zipfile.getEntries();
            while (e.hasMoreElements()) {
                countProcessedFiles++;
                countProgress = (int) (countProcessedFiles / countFiles * 100);
                SwingUtilities.invokeLater(new UpdateProgressBarTask(
                        DownloadApacheTomcat.jProgressBar1, countProgress));

                DownloadApacheTomcat.jProgressBar1.repaint();
                count = 0;
                data = new byte[BUFFER];
                entry = (ZipEntry) e.nextElement();

                localFileName = destinationFolder + entry.getName();
                if (!localFileName.endsWith("/")) {
                    //System.out.println("Extracting " + localFileName);

                    entryStream = zipfile.getInputStream(entry);

                    is = new BufferedInputStream(entryStream);
                    fos = new FileOutputStream(localFileName);
                    dest = new BufferedOutputStream(fos, BUFFER);

                    while ((count = is.read(data, 0, BUFFER)) != -1) {
                        dest.write(data, 0, count);
                    }

                    dest.flush();
                    dest.close();
                    is.close();
                    entryStream.close();
                }
            }
            zipfile.close();

            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    DownloadApacheTomcat.jProgressBar1.setValue(100);
                }
            });
            UnZipDone = true;
            UnZipWebArchive unZipWebArchive = new UnZipWebArchive();
            unZipWebArchive.ExtractWebArchive();

            parentForm.setVisible(true);
            downloadForm.setVisible(false);

        } catch (Exception ex) {
            LectorInstallerApp.logger.fatal(ex.getMessage(), ex);
        }
    }

    private class UpdateProgressBarTask implements Runnable {

        private JProgressBar progressBar;
        private int value;

        public UpdateProgressBarTask(JProgressBar progressBar, int value) {
            this.value = value;
            this.progressBar = progressBar;
        }

        @Override
        public void run() {
            if (this.progressBar != null) {
                this.progressBar.setValue(value);
            }
        }
    }
}
