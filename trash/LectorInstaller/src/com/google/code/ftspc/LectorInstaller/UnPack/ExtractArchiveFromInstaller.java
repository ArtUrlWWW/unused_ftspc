/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.google.code.ftspc.LectorInstaller.UnPack;

import com.google.code.ftspc.LectorInstaller.LectorInstallerApp;
import com.google.code.ftspc.LectorInstaller.MainFrame;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class ExtractArchiveFromInstaller {
    public void extract(String destination, String fileName) {
        OutputStream out = null;
        MainFrame.jLabel7.setText("Unpacking "+fileName+".");
        try {
            File f = new File(destination);
            InputStream inputStream =
                    MainFrame.class.getResourceAsStream(
                    fileName);
            out = new FileOutputStream(f);
            byte[] buf = new byte[1024];
            int len;
            while ((len = inputStream.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            inputStream.close();

        } catch (Exception ex) {
            LectorInstallerApp.logger.fatal(ex.getMessage(), ex);
        }
    }
    
}
