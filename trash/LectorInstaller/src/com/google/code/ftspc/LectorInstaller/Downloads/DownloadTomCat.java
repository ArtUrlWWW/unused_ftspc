package com.google.code.ftspc.LectorInstaller.Downloads;

import com.google.code.ftspc.LectorInstaller.SomeFunctions.InfoThread;
import com.google.code.ftspc.LectorInstaller.MainFrame;
import com.google.code.ftspc.LectorInstaller.UnPack.ExtractArchiveFromInstaller;
import com.google.code.ftspc.LectorInstaller.UnPack.UnZipTomCat;
import java.net.URL;
import java.util.List;
import java.util.Timer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;
import org.jdesktop.application.Application;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class DownloadTomCat extends Thread {

    private String urlString = null;
    private MainFrame parentForm;
    private DownloadApacheTomcat downloadForm;
    public static Boolean downloadDone = false;

    public DownloadTomCat(MainFrame parentForm, DownloadApacheTomcat downloadForm) {
        this.parentForm = parentForm;
        this.downloadForm = downloadForm;
    }

    @Override
    public void run() {
        DownloadFile downloadFile = new DownloadFile();
        String fileName = "apache-tomcat-7.0.16.zip";
        try {
            Timer InfoThreadTimer = new Timer();
            InfoThreadTimer.schedule(new InfoThread(), 0, 1000);

//            DownloadApacheTomcat.jLabel2.setText("Getting external properties");
//            String fileName = downloadFile.download("http://ftspc.googlecode.com/svn/trunk/System/LectorInstaller/test/externallinks.ini");
//            Properties externalProps = new Properties();
//            externalProps.load(new FileInputStream(MainFrame.destinationFolder + "/" + fileName));


            if (MainFrame.downloadTomcatFlag.isSelected()) {

                Pattern pattern = Pattern.compile("^http://.*/tomcat/.*bin/apache-tomcat-[[0-9]+\\.]+zip");
                Source source = new Source(new URL("http://tomcat.apache.org/download-70.cgi"));
                source.setLogger(null);
                source.fullSequentialParse();
                List<Element> linkElements = source.getAllElements(HTMLElementName.A);

                for (Element linkElement : linkElements) {
                    String href = linkElement.getAttributeValue("href");
                    if (href != null) {
                        Matcher matcher = pattern.matcher(href);
                        if (matcher.matches()) {
                            urlString = href;
                        }
                    }
                }

                DownloadApacheTomcat.jLabel2.setText("Downloading Apache Tomcat");
                if (urlString == null) {
                    JOptionPane.showMessageDialog(null, "Can't download TomCat. "
                            + "Please check your Internet connection and restart "
                            + "Installer.", "LectorInstaller", 1);
                    Application.getInstance().exit();
                }
                fileName = downloadFile.download(urlString);
            } else {
                DownloadApacheTomcat.jLabel2.setText("Extracting Tomcat archive");
                ExtractArchiveFromInstaller extractor = new ExtractArchiveFromInstaller();
                extractor.extract(MainFrame.destinationFolder + "/" + fileName, fileName);
            }
            downloadDone = true;
            DownloadApacheTomcat.jLabel2.setText("Unpacking Tomcat archive");
            UnZipTomCat UnZipTomCat = new UnZipTomCat(fileName, downloadForm, parentForm);
            UnZipTomCat.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}