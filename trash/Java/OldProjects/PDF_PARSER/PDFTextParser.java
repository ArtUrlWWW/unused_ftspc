import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import java.io.File;
import java.io.FileInputStream;

public class PDFTextParser implements Runnable
{
    
    public boolean busy = true;
    private boolean run = false;
    public int myid = -1;
    public String id = "";
    public String fileName = null;
    public boolean upd = false;
    private Thread th = null;
    
    public PDFTextParser(int i)
    {
        myid = i;
        busy = false;
        run  = true;
        th = new Thread(this);
//        th.setPriority(th.MAX_PRIORITY);
        th.start();
    }
    
    protected void startApp() {
        run = true;
    }

    protected void destroyApp(boolean flag) {
        run = false;
    }

    public void run()
    {
        while (run)
        {
            if (busy & fileName != null)
            {
                String s = null;
                PDFHandler.fixing(id);
                try
                {
                    s = pdftoText();
                }
                catch(Throwable e)
                {
                    System.out.println("A problem occured while parsed PDF " + fileName + "(id = " + id +") "+e.getMessage());
                }                
                PDFHandler.save(id, s, upd);                
                fileName = null;
                s = null;
                busy = false;
                try
                {
                    System.gc();
                }
                catch(Throwable e1)
                {
                    System.out.println("A problem occured while GC running " + fileName + "(id = " + id +") "+e1.getMessage());
                }
            }      
            else
            {
                try
                {
                    th.sleep(50);
                }
                catch(Throwable e)
                {
                }
            }
        }
    }

/*
    public void startParsing(int id, String fileName, boolean upd)
    {
        busy = true;
        this.id  = id;
        this.upd = upd;
        this.fileName = fileName;
    }
*/

    private String pdftoText()
    {
        String parsedText = null;
        PDFParser parser = null;
        PDFTextStripper pdfStripper = null;
        PDDocument pdDoc = null;
        COSDocument cosDoc = null;

        System.out.println("  Parsing text from PDF file " + fileName + "(id = " + id +")");

        File f = new File(fileName);  
        if (!f.isFile())
        {
            System.out.println("File " + fileName + " does not exist (id = " + id +")");
        }
        else
            try
            {
                System.out.println("Start");
                parser = new PDFParser(new FileInputStream(f));
                try 
                {
                    System.out.println("Starting");
                    parser.parse();
                    System.out.println("1");
                    cosDoc = parser.getDocument();
                    System.out.println("2");
                    pdfStripper = new PDFTextStripper();
                    System.out.println("3");
                    pdDoc = new PDDocument(cosDoc);
                    System.out.println("4");
                    parsedText = pdfStripper.getText(pdDoc);
                    System.out.println("5");
                }
                catch (Exception e) 
                {
                    System.out.println("An exception occured in parsing the PDF Document (id = " + id +") "+e.getMessage());
                }
                System.out.println("End");      
                try
                {
                    if (cosDoc != null) cosDoc.close();
                    if (pdDoc != null) pdDoc.close();
                } 
                catch (Exception e1) 
                {
                     System.out.println(e1.getMessage());
                }
                finally
                {
                    cosDoc = null;
                    pdDoc  = null;
                }
            } 
            catch (Exception e2) 
            {
                System.out.println("Unable to open PDF Parser (id = " + id +")");
            }
        
        return parsedText;
    }


}