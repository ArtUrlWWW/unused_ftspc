import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import java.io.File;
import java.io.FileInputStream;

public class PDFTest
{
    
    public static void main(String[] args)
    { 
        System.out.println(pdftoText(args[0]));
    }    

    private static String pdftoText(String fileName)
    {
        String parsedText = null;
        PDFParser parser = null;
        PDFTextStripper pdfStripper = null;
        PDDocument pdDoc = null;
        COSDocument cosDoc = null;

        File f = new File(fileName);  
        if (!f.isFile())
        {
            System.out.println("File " + fileName + " does not exist.");
        }
        else
            try
            {
                parser = new PDFParser(new FileInputStream(f));
                try 
                {
                    parser.parse();
System.out.println("* 1 *");
                    cosDoc = parser.getDocument();
System.out.println("* 2 *");
                    pdfStripper = new PDFTextStripper();
                    pdDoc = new PDDocument(cosDoc);
                    parsedText = pdfStripper.getText(pdDoc); 
                }
                catch (Throwable e) 
                {
                    System.out.println("An exception occured in parsing the PDF Document "+e.getMessage());
                }      
                try
                {
                    if (cosDoc != null) cosDoc.close();
                    if (pdDoc != null) pdDoc.close();
                } 
                catch (Exception e1) 
                {
                  System.out.println(e1.getMessage());
                }
                finally
                {
                    cosDoc = null;
                    pdDoc  = null;
                }
            } 
            catch (Exception e2) 
            {
                System.out.println("Unable to open PDF Parser");
            }
        return parsedText;
    }


}