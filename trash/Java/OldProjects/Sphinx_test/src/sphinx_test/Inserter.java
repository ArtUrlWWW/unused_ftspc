package sphinx_test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Inserter {

    public static void insert(String hashoffile, String name, String path, double file_sizeee, String filesizebyte, Connection conn) {

        String todo = "";
        //Connection conn;

        try {
            //conn = getConnection();
            java.sql.Statement s = conn.createStatement();

            path=path.replace("\\", "/");
            path=path.replace("//", "/");


            todo = "INSERT into files " + "(id, name, path, fullpath, filesize, filesizebyte)"
                    + "values ('" + hashoffile + "', '" + name + "', '" + path + "/', '" + path + "/" + name + "', '"
                    + filesizebyte + "', " + file_sizeee + ")";

            try {
                int r = s.executeUpdate(todo);
            } catch (Exception e) {
                System.out.println("Can't insert to MySQL");
                System.out.println(e.toString());
            }
            //conn.close();
            s.close();
        } catch (Exception ex) {
            System.out.println("Can't connect to MySQL");
            System.out.println(ex.toString());
        }
    }

    public static String selecter(Connection conn, Integer id) {
        String todo = "";
        String content_text="";
        ResultSet rs = null;

        try {
            Statement s = conn.createStatement();          

            todo="select content from documents_new where id="+id;
            //System.out.println(todo);

            try {
                rs = s.executeQuery(todo);
                rs.next();
                content_text = rs.getString(1);
            } catch (Exception e) {
                System.out.println("Can't select from MySQL");
                System.out.println(e.toString());
            }
            //conn.close();
            s.close();
        } catch (Exception ex) {
            System.out.println("Can't connect to MySQL");
            System.out.println(ex.toString());
        }
        return content_text;
    }

    public static Connection getConnection() throws Exception {
        String driver = "org.gjt.mm.mysql.Driver";
        String url = "jdbc:mysql://192.168.1.55/test";
        String username = "root";
        String password = "7";

        Class.forName(driver); // load MySQL driver
        Connection conn = DriverManager.getConnection(url, username, password);
        return conn;
    }
}
