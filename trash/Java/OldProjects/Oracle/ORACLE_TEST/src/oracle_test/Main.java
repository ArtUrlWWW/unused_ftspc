/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package oracle_test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;

import oracle.jdbc.OracleResultSet;

/**
 *
 * @author AKhusnutdinov
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        Connection connection = null;

        try {
            // Load the JDBC driver
            String driverName = "oracle.jdbc.driver.OracleDriver";
            Class.forName(driverName);

            // Create a connection to the database
            String serverName = "127.0.0.1";
            String portNumber = "1521";
            String sid = "AKHUSNUT";
            String url = "jdbc:oracle:thin:@" + serverName + ":" + portNumber + ":" + sid;
            String username = "SYSTEM";
            String password = "a1";
            connection = DriverManager.getConnection(url, username, password);

            //String query = "SELECT BID, BNAME, BLOCATION FROM banks";
            String query = "SELECT * FROM banks";
            String query1 = "insert into banks(BID, BNAME, BLOCATION) values(?, ?, ?)";

            pstmt = connection.prepareStatement(query1);

            pstmt.setInt(1, 3006); // set input parameter 1
            pstmt.setString(2, "qaz"); // set input parameter 2
            pstmt.setString(3, "rrr"); // set input parameter 3

            pstmt.executeUpdate();

            pstmt = connection.prepareStatement(query); // create a statement

            // create a statement
//      pstmt.setInt(1, 1001); // set input parameter
            rs = pstmt.executeQuery();
            // extract data from the ResultSet
            while (rs.next()) {
                int dbDeptNumber = rs.getInt(1);
                String dbDeptName = rs.getString(2);
                String dbDeptLocation = rs.getString(3);
                System.out.println(dbDeptNumber + "\t" + dbDeptName + "\t" + dbDeptLocation);
            }


        } catch (SQLException e) {
            // Could not connect to the database
            System.out.println("!!!" + e.toString());
        }
    }
}
