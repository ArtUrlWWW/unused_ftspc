package local_doc_parser;

import java.io.File;
import java.io.FileInputStream;
//import org.apache.poi.POIDocument;
//import org.apache.poi.POITextExtractor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.hssf.extractor.ExcelExtractor;
import org.apache.poi.hdgf.extractor.VisioTextExtractor;
import org.apache.poi.hslf.extractor.PowerPointExtractor;

public class MSTextParser implements Runnable {

    public boolean busy = true;
    private boolean run = false;
    public int myid = -1;
    public String id = "";
    public String putkfaylu = null;
    private Thread th = null;
    public String Outputpath = null;
    public String hashfayla = "";
    public String host = null;
    public String dbName = null;
    public String userrr = null;
    public String pass = null;
    public String Filestype = "";
    Local_doc_parserView view;

    public MSTextParser(Local_doc_parserView view) {
        this.view = view;
    }

    public void save(String hash, String s) throws FileNotFoundException, IOException {
        OutputStream f0 = new FileOutputStream(Outputpath + hash + ".txt");
        if (s != null) {
            f0.write(s.getBytes("UTF-8"));
            f0.close();
            Inserter.insert(hash, host, dbName, userrr, pass);
        }
    }

    public MSTextParser(int i) {
        myid = i;
        busy = false;
        run = true;
        th = new Thread(this);
        th.start();
    }

    protected void startApp() {
        run = true;
    }

    protected void destroyApp(boolean flag) {
        run = false;
    }

    public void run() {
        int zapisey = 0;
        while (run) {
            if (busy & putkfaylu != null) {
                String s = null;

                try {
                    hashfayla = ForMD5.getmemd5(putkfaylu);
                    zapisey = Inserter.countRows(hashfayla, host, dbName, userrr, pass);
                    if (zapisey > 0) {
                        s = "777";
                        //System.out.println("Файл с хешем " + hashfayla + " уже проиндексен");
                    } else {
                        try {
                            s = docToText(Filestype);
                        } catch (IOException ex) {
                            System.out.println("Error # MSTP1 " + ex.toString());
                        } catch (Exception ex) {
                            System.out.println("Error # MSTP2 " + ex.toString());
                        }
                    }
                } catch (Throwable e) {
                    System.out.println("A problem occured while parsed MS document " + putkfaylu + "(id = " + id + ") " + e.getMessage());
                }
                try {
                    if (s!=null) {

                        if (s.trim().length() > 20) {
                            save(hashfayla, s);
                        }
                    }
                } catch (FileNotFoundException ex) {
                    System.out.println(ex.toString());
                } catch (IOException ex) {
                    System.out.println(ex.toString());
                }

                putkfaylu = null;
                s = null;
                hashfayla = null;
                busy = false;

                try {
                    Local_doc_parserView.proparsennihfaylov++;
                } catch (Exception ex) {
                    System.out.println(ex.toString());
                }

                try {
                    System.gc();
                } catch (Throwable e1) {
                    System.out.println("A problem occured while GC running " + putkfaylu + "(id = " + id + ") " + e1.getMessage());
                }
            } else {
                try {
                    th.sleep(50);
                } catch (Throwable e) {
                }
            }
        }
    }

    private String docToText(String typeee) throws Exception {
        String res = "";
        PDFParser parser = null;
        PDFTextStripper pdfStripper = null;
        PDDocument pdDoc = null;
        COSDocument cosDoc = null;
        File f = new File(putkfaylu);
        if (!f.isFile()) {
            System.out.println("File " + putkfaylu + " does not exist.");
        } else {
//                res = POITextExtractor(new POIDocument(new POIFSFileSystem(new FileInputStream(f)))).getText();
//            try
//            {

            if (Filestype.equals(".doc")
                    || Filestype.equals(".xls")
                    || Filestype.equals(".ptt")
                    || Filestype.equals(".pss")) {
                String ext = putkfaylu.substring(putkfaylu.length() - 3).toUpperCase();
                if (ext.equals("DOC")) {
                    res = (new WordExtractor(new FileInputStream(f)).getText());
                } else if (ext.equals("XLS")) {
                    res = (new ExcelExtractor(new POIFSFileSystem(new FileInputStream(f))).getText());
                } else if (ext.equals("PPT") || ext.equals("PPS")) {
                    res = (new PowerPointExtractor(new FileInputStream(f)).getText());
                } else {
                    res = (new VisioTextExtractor(new FileInputStream(f)).getText());
                }
//            }
//            catch (Exception e)
//            {
//                System.out.println("Document parsing error: "+e.getMessage());
//            }
            }
            if (Filestype.equals(".pdf")) {
                try {
                    parser = new PDFParser(new FileInputStream(f));
                    try {
                        parser.parse();
                        cosDoc = parser.getDocument();
                        pdfStripper = new PDFTextStripper();
                        pdDoc = new PDDocument(cosDoc);
                        try {
                            res = pdfStripper.getText(pdDoc);
                        } catch (IOException ex) {
                            System.out.println(ex.toString());
                        }
                    } catch (Exception e) {
                        System.out.println("An exception occured in parsing the PDF Document (id = " + id + ") " + e.getMessage());
                    }
                    try {
                        if (cosDoc != null) {
                            cosDoc.close();
                        }
                        if (pdDoc != null) {
                            pdDoc.close();
                        }
                    } catch (Exception e1) {
                        System.out.println(e1.getMessage());
                    } finally {
                        cosDoc = null;
                        pdDoc = null;
                    }
                } catch (Exception e2) {
                    System.out.println("Unable to open PDF Parser (id = " + id + ")");
                }
            }
        }
        return res;
    }
}
