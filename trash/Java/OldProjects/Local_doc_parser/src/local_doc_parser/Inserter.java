package local_doc_parser;

import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Inserter {

    public static void insert(String hashoffile, String hostnamee, String databasename, String username, String password) {

        String todo = "";
        java.sql.Connection conn;

        try {
            conn = getConnection(hostnamee, databasename, username, password);
            java.sql.Statement s = conn.createStatement();

            todo = "INSERT into parsedfiles " + "(hashoffile)"
                    + "values ('" + hashoffile + "'" + ")";

            try {

                int r = s.executeUpdate(todo);
            } catch (Exception e) {
                System.out.println("Can't insert to MySQL");
                System.out.println(e.toString());
            }
            conn.close();
            s.close();
        } catch (Exception ex) {
            System.out.println("Can't connect to MySQL");
            System.out.println(ex.toString());
        }
    }

    public static int countRows(String hashoffile, String hostnamee, String databasename, String username, String password) throws SQLException {
        // select the number of rows in the table
        Statement stmt = null;
        ResultSet rs = null;
        int rowCount = -1;
        try {
            Connection conn = getConnection(hostnamee, databasename, username, password);
            stmt = (Statement) conn.createStatement();
            rs = stmt.executeQuery("SELECT COUNT(*) FROM parsedfiles where hashoffile='" + hashoffile + "'");
            // get the number of rows from the result set
            rs.next();
            rowCount = rs.getInt(1);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            rs.close();
            stmt.close();
        }
        return rowCount;
    }

    public static Connection getConnection(String hostnamee, String databasename, String username, String password) throws Exception {
        String driver = "org.gjt.mm.mysql.Driver";
        String url = "jdbc:mysql://" + hostnamee + "/" + databasename;
        Class.forName(driver); // load MySQL driver
        Connection conn = DriverManager.getConnection(url, username, password);
        return conn;
    }
}
