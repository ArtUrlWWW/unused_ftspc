package local_doc_parser;

import java.io.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ForMD5 {
    public static String getmemd5(String vhodnoyfayl) throws NoSuchAlgorithmException, FileNotFoundException {
        String output = "";

        MessageDigest digest = MessageDigest.getInstance("MD5");
        File f = new File(vhodnoyfayl);
        InputStream is = new FileInputStream(f);
        byte[] buffer = new byte[7777777];
        int read = 0;
        try {
            while ((read = is.read(buffer)) > 0) {
                digest.update(buffer, 0, read);
            }
            byte[] md5sum = digest.digest();
            BigInteger bigInt = new BigInteger(1, md5sum);
            output = bigInt.toString(16);
        }
        catch (IOException e) {
            throw new RuntimeException("Unable to process file for MD5", e);
        }
        finally {
            try {
                is.close();
            }
            catch (IOException e) {
                throw new RuntimeException("Unable to close input stream for MD5 calculation", e);
            }
        }
        return output;
    }
}
