/*
 * Local_doc_parserView.java
 */
package local_doc_parser;

import org.jdesktop.application.Action;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskMonitor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import java.io.*;
import java.util.*;

/**
 * The application's main frame.
 */
public class Local_doc_parserView extends FrameView {

    public static int records = 0;
    public static int counter = 0;
    public static int Part = 500; // Connection will be rebuild after handling this number of records
    public static int Len = 50;
    public String host = null;
    public String dbName = null;
    public String userrr = null;
    public String pass = null;
    public static String Outputpath = null;
    public static String Inputpath = null;
    public static MSTextParser[] parser = null;
    //public String Fileshash[] = new String[77777];
    public String Filespath[] = new String[77777];
    public String Filestype[] = new String[77777];
    public int chyotchik = 0;
    public static int obstcheekolvofaylov = 0;
    public static int proparsennihfaylov = 0;
    public int delay = 1000; //milliseconds
    ActionListener taskPerformer = new ActionListener() {

        public void actionPerformed(ActionEvent evt) {
            float proggress=((float)proparsennihfaylov/(float)obstcheekolvofaylov)*100;
            jProgressBar1.setValue((int)proggress);
            jLabel1.setText(String.valueOf(jProgressBar1.getValue())+" %");
        }
    };

    public void spiderrr(String secondDirectory) {
        String resultat = "";
        String list[] = new File(secondDirectory).list();
        String tempmd5 = "";
        String imyafayla = "";
        String tipfayla = "";

        for (int i = 0; i < list.length; i++) {
            resultat = list[i];
            File f1 = new File(secondDirectory + "/" + resultat);
            //System.out.println("File: " + secondDirectory + "/" + resultat);
            imyafayla = f1.getName().toLowerCase().toString();

            if (f1.isFile()) {
                if (imyafayla.length() > 4) {
                    tipfayla = imyafayla.substring(imyafayla.length() - 4, imyafayla.length());
                    if (tipfayla.equals(".doc")
                            || tipfayla.equals(".xls")
                            || tipfayla.equals(".ptt")
                            || tipfayla.equals(".pss")
                            || tipfayla.equals(".pdf")) {
                        //System.out.println("File: " + secondDirectory + "/" + resultat);
                        try {
                            obstcheekolvofaylov++;
                            Filespath[chyotchik] = secondDirectory + "/" + resultat;
                            Filestype[chyotchik] = tipfayla;

                        } catch (Exception ex) {
                            System.out.println(ex.toString());
                        }
                        chyotchik++;
                    }//
                }//
            }
            if (f1.isDirectory()) {
                //System.out.println("SubDirectory " + resultat);
                spiderrr(secondDirectory + "/" + resultat);
            }
        }
    }

    public Local_doc_parserView(SingleFrameApplication app) {
        super(app);

        initComponents();

        // status bar initialization - message timeout, idle icon and busy animation, etc
        ResourceMap resourceMap = getResourceMap();
        int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
        messageTimer = new Timer(messageTimeout, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                statusMessageLabel.setText("");
            }
        });
        messageTimer.setRepeats(false);
        int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
        for (int i = 0; i < busyIcons.length; i++) {
            busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
        }
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
                statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
            }
        });
        idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
        statusAnimationLabel.setIcon(idleIcon);
        progressBar.setVisible(false);

        // connecting action tasks to status bar via TaskMonitor
        TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
        taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener() {

            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                String propertyName = evt.getPropertyName();
                if ("started".equals(propertyName)) {
                    if (!busyIconTimer.isRunning()) {
                        statusAnimationLabel.setIcon(busyIcons[0]);
                        busyIconIndex = 0;
                        busyIconTimer.start();
                    }
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(true);
                } else if ("done".equals(propertyName)) {
                    busyIconTimer.stop();
                    statusAnimationLabel.setIcon(idleIcon);
                    progressBar.setVisible(false);
                    progressBar.setValue(0);
                } else if ("message".equals(propertyName)) {
                    String text = (String) (evt.getNewValue());
                    statusMessageLabel.setText((text == null) ? "" : text);
                    messageTimer.restart();
                } else if ("progress".equals(propertyName)) {
                    int value = (Integer) (evt.getNewValue());
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(false);
                    progressBar.setValue(value);
                }
            }
        });
    }

    @Action
    public void showAboutBox() {
        if (aboutBox == null) {
            JFrame mainFrame = Local_doc_parserApp.getApplication().getMainFrame();
            aboutBox = new Local_doc_parserAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        Local_doc_parserApp.getApplication().show(aboutBox);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
        statusPanel = new javax.swing.JPanel();
        javax.swing.JSeparator statusPanelSeparator = new javax.swing.JSeparator();
        statusMessageLabel = new javax.swing.JLabel();
        statusAnimationLabel = new javax.swing.JLabel();
        progressBar = new javax.swing.JProgressBar();
        jProgressBar1 = new javax.swing.JProgressBar();
        jLabel1 = new javax.swing.JLabel();

        mainPanel.setName("mainPanel"); // NOI18N

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(local_doc_parser.Local_doc_parserApp.class).getContext().getActionMap(Local_doc_parserView.class, this);
        jButton1.setAction(actionMap.get("start_parsing")); // NOI18N
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(local_doc_parser.Local_doc_parserApp.class).getContext().getResourceMap(Local_doc_parserView.class);
        jButton1.setText(resourceMap.getString("jButton1.text")); // NOI18N
        jButton1.setName("jButton1"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jTextArea1.setName("jTextArea1"); // NOI18N
        jScrollPane1.setViewportView(jTextArea1);

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
                .addContainerGap())
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(jButton1))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 222, Short.MAX_VALUE)))
                .addContainerGap())
        );

        menuBar.setName("menuBar"); // NOI18N

        fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
        fileMenu.setName("fileMenu"); // NOI18N

        exitMenuItem.setAction(actionMap.get("quit")); // NOI18N
        exitMenuItem.setName("exitMenuItem"); // NOI18N
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
        helpMenu.setName("helpMenu"); // NOI18N

        aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        statusPanel.setName("statusPanel"); // NOI18N

        statusPanelSeparator.setName("statusPanelSeparator"); // NOI18N

        statusMessageLabel.setName("statusMessageLabel"); // NOI18N

        statusAnimationLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        statusAnimationLabel.setName("statusAnimationLabel"); // NOI18N

        progressBar.setName("progressBar"); // NOI18N

        jProgressBar1.setName("jProgressBar1"); // NOI18N

        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusPanelSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(statusMessageLabel)
                    .addGroup(statusPanelLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jProgressBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusAnimationLabel)
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, statusPanelLayout.createSequentialGroup()
                .addComponent(statusPanelSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(statusMessageLabel)
                        .addComponent(statusAnimationLabel)
                        .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        setComponent(mainPanel);
        setMenuBar(menuBar);
        setStatusBar(statusPanel);
    }// </editor-fold>//GEN-END:initComponents

    @Action
    public Task start_parsing() {
        return new Start_parsingTask(getApplication());
    }

    private class Start_parsingTask extends org.jdesktop.application.Task<Object, Void> {

        Start_parsingTask(org.jdesktop.application.Application app) {
            // Runs on the EDT.  Copy GUI state that
            // doInBackground() depends on from parameters
            // to Start_parsingTask fields, here.
            super(app);
        }

        @Override
        protected Object doInBackground() {
            new Timer(delay, taskPerformer).start();

            try {
                Properties prop = new Properties();
                prop.load(new FileInputStream("MSHandler.ini"));
                host = prop.getProperty("Host");
                dbName = prop.getProperty("DBName");
                userrr = prop.getProperty("User");
                pass = prop.getProperty("Password");
                counter = Integer.parseInt(prop.getProperty("Threads"));
                Part = Integer.parseInt(prop.getProperty("Part"));
                Len = Integer.parseInt(prop.getProperty("Length"));
                Outputpath = prop.getProperty("Outputpath");
                Inputpath = prop.getProperty("Inputpath");
            } catch (Exception e) {
                System.out.println("Reading ini-file error..." + e.getMessage());
                System.exit(0);
            }
            MSTextParser[] parser = new MSTextParser[counter];
            for (int i = 0; i < counter; i++) {
                parser[i] = new MSTextParser(i);
            }

            jTextArea1.append("Главная папка: " + Inputpath + "\r\n");
            jTextArea1.append("Ищу файлы..." + "\r\n");
            spiderrr(Inputpath);
            jTextArea1.append("Всего файлов: " + obstcheekolvofaylov + "\r\n");
            jTextArea1.append("Начал парсить файлы..." + "\r\n");

            for (int x = 0; x < chyotchik; x++) {
                //System.out.println(Filespath[x] + "---" + Fileshash[x]);
                try {
                    boolean b = true;
                    while (b) {
                        for (int i = 0; i < counter; i++) {
                            b = parser[i].busy;
                            if (!b) {
                                parser[i].busy = true;
                                parser[i].Outputpath = Outputpath;
                                parser[i].putkfaylu = Filespath[x];
                                parser[i].host = host;
                                parser[i].dbName = dbName;
                                parser[i].userrr = userrr;
                                parser[i].pass = pass;
                                parser[i].Filestype = Filestype[x];
                                break;
                            }
                        }
                    }
                } catch (Exception e7) {
                    System.out.println("Ошибка записи в поток: " + e7.toString());
                }
            }

            //////////////////////////////////////////////////////////////////////////////
            for (int i = 0; i < counter; i++) {
                if (parser[i] != null) {
                    while (parser[i].busy) {
                        try {
                            Thread.sleep(100);
                        } catch (Exception e8) {
                        }
                    }
                    parser[i].destroyApp(true);
                }
            }
            jTextArea1.append("Файлы пропарсены." + "\r\n");
            System.gc();
            jTextArea1.append("Программа завершила свою работу." + "\r\n");
            return null;  // return your result
        }

        @Override
        protected void succeeded(Object result) {
            // Runs on the EDT.  Update the GUI based on
            // the result computed by doInBackground().
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    public javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JLabel statusAnimationLabel;
    private javax.swing.JLabel statusMessageLabel;
    private javax.swing.JPanel statusPanel;
    // End of variables declaration//GEN-END:variables
    private final Timer messageTimer;
    private final Timer busyIconTimer;
    private final Icon idleIcon;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;
    private JDialog aboutBox;
}
