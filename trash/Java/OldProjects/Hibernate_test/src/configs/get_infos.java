/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package configs;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author AKhusnutdinov
 */
public class get_infos {

    private static String QUERY_BASED_ON_STR_ID = "from Faylovvdirektorii a where a.dirid between 1 and 100";

    public void runQueryBasedOnFirstName(String str_id) {
        //executeHQLQuery(QUERY_BASED_ON_STR_ID+str_id);
        executeHQLQuery(QUERY_BASED_ON_STR_ID);
    }

    private void executeHQLQuery(String hql) {
        try {

            Session session = NewHibernateUtil.getSessionFactory().openSession();

            /*   Faylovvdirektorii Faylovvdirektorii_1 = new Faylovvdirektorii();

            Faylovvdirektorii_1.setDirid(7);
            Faylovvdirektorii_1.setKolvofaylov(777);
            session.saveOrUpdate(Faylovvdirektorii_1);*/


            session.beginTransaction();
            Query q = session.createQuery(hql);
            List resultList = q.list();
            displayResult(resultList);
            //System.out.println(resultList);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }

    }

    private void displayResult(List resultList) {

        for (Object o : resultList) {
            Faylovvdirektorii Faylovvdirektorii = (Faylovvdirektorii) o;
            System.out.println(Faylovvdirektorii.getDirid() + " - " + Faylovvdirektorii.getKolvofaylov());
        }

    }
}
