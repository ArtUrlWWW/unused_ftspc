/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sqlite_jdbc_test;
import java.sql.*;
/**
 *
 * @author Administrator
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception  {
        // TODO code application logic here
         Class.forName("org.sqlite.JDBC");
         Connection conn =DriverManager.getConnection("jdbc:sqlite:C:/test.db");
         Statement stat = conn.createStatement();
         stat.executeUpdate("drop table if exists people;");
         stat.executeUpdate("create table people (name, occupation);");
         PreparedStatement prep = conn.prepareStatement(
      "insert into people values (?, ?);");
 
    prep.setString(1, "Wittgenstein");
    prep.setString(2, "smartypants");
    prep.addBatch();

    conn.setAutoCommit(false);
    prep.executeBatch();
    conn.setAutoCommit(true);

    ResultSet rs = stat.executeQuery("select * from people;");
    while (rs.next()) {
      System.out.println("name = " + rs.getString("name"));
      System.out.println("job = " + rs.getString("occupation"));
    }
    rs.close();
    conn.close();
    }

}
