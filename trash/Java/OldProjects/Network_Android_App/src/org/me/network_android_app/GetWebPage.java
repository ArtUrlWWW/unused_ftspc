/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.me.network_android_app;

import android.app.Activity;
import android.os.Bundle;
// used for interacting with user interface
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.view.View;
// used for passing data
import android.os.Handler;
import android.os.Message;
// used for connectivity
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class GetWebPage extends Activity {

    int trig = 0;
    Handler h;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        final EditText eText = (EditText) findViewById(R.id.address);
        final TextView tView = (TextView) findViewById(R.id.pagetext);

        this.h = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 0:
                        tView.append((String) msg.obj);
                        break;
                }
                super.handleMessage(msg);
            }
        };
        final Button button = (Button) findViewById(R.id.ButtonGo);
        button.setOnClickListener(new Button.OnClickListener() {

            public void onClick(View v) {
                String addr = eText.getText().toString();
                System.setProperty("http.proxyHost", "172.30.13.3");
                System.setProperty("http.proxyPort", "8080");


                try {

                    if (trig < 1) {
                        tView.setTextColor(R.color.new_color);
                        trig = 1;
                    } else {
                        tView.setTextColor(R.color.opaque_blue);
                        trig = 0;
                    }
                    tView.setText("ТЕСТ 1" + "\n");
                    tView.append(String.valueOf(trig));

                    tView.append(getString(R.string.app_name) + "\n");
                    System.setProperty("java.net.useSystemProxies", "true");
                    URL url = new URL(addr);
                    URLConnection conn = url.openConnection();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    String line = "";
                    while ((line = rd.readLine()) != null) {
                        Message lmsg;
                        lmsg = new Message();
                        lmsg.obj = line;
                        lmsg.what = 0;
                        GetWebPage.this.h.sendMessage(lmsg);
                    }
                } catch (Exception e) {
                }
            }
        });
    }
}
