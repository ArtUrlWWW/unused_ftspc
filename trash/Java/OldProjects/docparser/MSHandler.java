import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.io.FileInputStream;
import java.util.Properties;
import java.io.*;
import java.util.*;

// Notice, do not import com.mysql.jdbc.*
// or you will have problems!

public class MSHandler 
{
  
    private static int records = 0;
    private static int counter = 0;
    private static int Part = 500; // Connection will be rebuild after handling this number of records
    private static int Len   = 50;
    private static String host = null;
    private static String dbName = null;
    private static String user = null;
    private static String pass = null;           
    private static String Outputpath = null;           
    private static MSTextParser[] parser = null;


    public static void main(String[] args)
    { 
        try
        {
            Properties prop = new Properties();
            prop.load(new FileInputStream("MSHandler.ini"));
            host    = prop.getProperty("Host");
            dbName  = prop.getProperty("DBName");
            user    = prop.getProperty("User");
            pass    = prop.getProperty("Password");           
            counter = Integer.parseInt(prop.getProperty("Threads"));
            Part    = Integer.parseInt(prop.getProperty("Part"));
            Len     = Integer.parseInt(prop.getProperty("Length"));
//            textSize= Integer.parseInt(prop.getProperty("Size"));
            Outputpath= prop.getProperty("Outputpath");
        }
        catch (Exception e)
        {
            System.out.println("Reading ini-file error..."+e.getMessage());
            System.exit(0);
        }

        int records  = Part;
        String[] ids    = new String[Part];
        String[] pth = new String[Part];
        parser       = new MSTextParser[counter];
        for (int i=0; i<counter; i++) parser[i]=new MSTextParser(i);
      try
      {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        while (records == Part) 
        try
        {
            records = 0;
            try
            {
                Connection cn = DriverManager.getConnection("jdbc:mysql://"+host+"/"+dbName+"?user="+user+"&password="+pass+"&autoReconnect=true");
                System.out.println("Select new part of records...");
                Statement st = null;
                ResultSet rs = null;
                try
                {
                    st = cn.createStatement();
                    rs = st.executeQuery("SELECT id,fullpath FROM files WHERE parsed=0 AND (name like '%.doc' OR name like '%.DOC' OR name like '%.xls' OR name like '%.XLS' OR name like '%.ppt' OR name like '%.PPT' OR name like '%.pps' OR name like '%.PPS' OR name like '%.vsd' OR name like '%.VSD' OR name like '%.vss' OR name like '%.VSS' OR name like '%.vst' OR name like '%.VST' OR name like '%.vdx' OR name like '%.VDX' OR name like '%.vsx' OR name like '%.VSX' OR name like '%.vtx' OR name like '%.VTX')");
//VSD, .VSS, .VST, .VDX, .VSX � .VTX - VISIO
                    while (rs.next() && (records<Part))
                    {
                        ids[records] = rs.getString("id");
                        pth[records] = rs.getString("fullpath");
                        records++;               
                    }
                }
                catch (SQLException e3)
                {
                    SQLExceptionHandler(e3,3);
                }
                finally 
                {
                    try { rs.close(); } catch (Exception e4) {} finally { rs = null; };
                    try { st.close(); } catch (Exception e5) {} finally { st = null; };
                    try { cn.close(); } catch (Exception e6) {} finally { cn = null; };
                }
            }
            catch (SQLException e1)
            {
                SQLExceptionHandler(e1,1);
            }
            if (records>0)
            {
                for (int ir=0; ir < records; ir++)
                {
                    Connection cn = null;
                    Statement  st = null;
                    ResultSet  rs = null;
                    try
                    {
                        cn = DriverManager.getConnection("jdbc:mysql://"+host+"/"+dbName+"?user="+user+"&password="+pass+"&autoReconnect=true");
                        st = cn.createStatement();
                        rs = st.executeQuery("SELECT count(*) c FROM parsedtexts WHERE id like '" + ids[ir]+"'");
                        if (rs.next())
                        {
                            boolean upd = (rs.getInt("c")>0);
                            boolean b = true;
                            while (b)
                            {
                                for(int i=0; i<counter; i++)
                                {
                                    b = parser[i].busy;
                                    if (!b) 
                                    {
                                        parser[i].busy = true;
                                        System.out.println("  Start parsing id=" + ids[ir] + " by parser " + i +" with upd=" + upd );
                                        parser[i].id  = ids[ir];
                                        parser[i].upd = upd;
                                        parser[i].fileName = pth[ir];
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    catch (SQLException e2)
                    {
                        System.out.println("Point 2. Problem with parsing id=" + ids[ir]+e2);
                    }
                    finally
                    {
                        try { rs.close(); } catch (Exception e4) {} finally { rs = null; };
                        try { st.close(); } catch (Exception e5) {} finally { st = null; };
                        try { cn.close(); } catch (Exception e6) {} finally { cn = null; };                          
                    }
                }
            }
            System.gc();
        }
        catch (Exception e7)
        {
            System.out.println("Point 7. Driver Loading Exception: " + e7.getMessage());
        }
      }
      catch(Exception ee)
      {
            System.out.println("Point 0. Driver Loading Exception: " + ee.getMessage());
      }
            for (int i=0; i<counter; i++)
            {
                if (parser[i] != null)
                {
                    while (parser[i].busy) 
                    try
                    {
                        Thread.sleep(100);
                    }
                    catch (Exception e8)
                    {
                    }
                    parser[i].destroyApp(true);
                }
            }
            System.gc();
    }

    public static void save(String id, String s, boolean upd)
    {
        Connection cn = null;
        Statement  s1 = null;
        Statement  s2 = null;
        PreparedStatement pst = null;
        try
        {
        cn = DriverManager.getConnection("jdbc:mysql://"+host+"/"+dbName+"?user="+user+"&password="+pass+"&autoReconnect=true");
        
        if (s != null) 
            {        
        OutputStream f0 = new FileOutputStream(Outputpath+id+".txt");
        //System.out.println(s);
        f0.write(s.getBytes("UTF-8"));
        f0.close();
        
        s = "UPDATE files SET parsed=1 WHERE id like '" + id+"'";
        s2 = cn.createStatement();
        int k = s2.executeUpdate(s);
        }
          /* cn = DriverManager.getConnection("jdbc:mysql://"+host+"/"+dbName+"?user="+user+"&password="+pass+"&autoReconnect=true");
            if (s != null) 
            {
                if (upd)
                {
                    try
                    { 
                        s1 = cn.createStatement();
                        int k = s1.executeUpdate("DELETE FROM parsedtexts WHERE id="+id);
                    }
                    catch(SQLException e10)
                    {
                        SQLExceptionHandler(e10,10);
                    }
                }
                s = s.trim();
                if (s.length()>Len)
                {
                  pst = cn.prepareStatement("INSERT INTO parsedtexts (id,parsedtext,part) VALUES (" + id + ", ?, ? )");
                  int j = 0;              
                  
                  
                  while (s.length() > 0)
                  {
                    int k = 0;
                    String ss = null;
                    if (s.length() > textSize)
                    {
                        ss = s.substring(0,textSize);
                        s = s.substring(textSize);
                    }
                    else
                    {
                        ss = s;
                        s = "";
                    }
                    try
                    {
                        pst.setString(1,ss);
                        pst.setInt(2,j);
                        j++;
                        k = pst.executeUpdate();
                        System.out.println("  "+k+" records in id="+id);
                    }
                    catch(SQLException e8)
                    {
                        SQLExceptionHandler(e8,8);
                        cn = null;
                        System.gc();
                        try
                        {
                            cn = DriverManager.getConnection("jdbc:mysql://"+host+"/"+dbName+"?user="+user+"&password="+pass+"&autoReconnect=true");
                        }
                        catch(Exception e20)
                        {
                            System.out.println("A problem with reconnection id="+id);
                            return;
                        }
                        s = "UPDATE files SET parsed=3 WHERE id=" + id;
                        break;
                    }
                  } 
                  if (s.length()==0) s = "UPDATE files SET parsed=1 WHERE id=" + id;
                }
                else
                {
                    s = "UPDATE files SET parsed=2 WHERE id=" + id;
                }
            }
            else
            {
                s = "UPDATE files SET parsed=2 WHERE id=" + id;
            }
            try
            {
                s2 = cn.createStatement();
                int k = s2.executeUpdate(s);
            }
            catch(SQLException e9)
            {
                SQLExceptionHandler(e9,9);
            }*/
        }
        catch(Exception e15)
        {
            System.out.println("Point 15. " + e15.getMessage());
        }
        finally
        {
            try { pst.close(); } catch (Exception e3) {} finally { pst = null; };
            try { s2.close(); } catch (Exception e4) {} finally { s2 = null; };
            try { s1.close(); } catch (Exception e5) {} finally { s1 = null; };
            try { cn.close(); } catch (Exception e6) {} finally { cn = null; };                          
        }
    }
    
    public static void fixing(String id)
    {
        Connection cn = null;
        Statement  s1 = null;
        Statement  s2 = null;
        PreparedStatement pst = null;
        try
        {
        cn = DriverManager.getConnection("jdbc:mysql://"+host+"/"+dbName+"?user="+user+"&password="+pass+"&autoReconnect=true");
        String s="";
        s = "UPDATE files SET parsed=2 WHERE id like '" + id+"'";
        s2 = cn.createStatement();
        int k = s2.executeUpdate(s);

         
        }
        catch(Exception e15)
        {
            System.out.println("Point 15. " + e15.getMessage());
        }
        finally
        {
            try { pst.close(); } catch (Exception e3) {} finally { pst = null; };
            try { s2.close(); } catch (Exception e4) {} finally { s2 = null; };
            try { s1.close(); } catch (Exception e5) {} finally { s1 = null; };
            try { cn.close(); } catch (Exception e6) {} finally { cn = null; };                          
        }
    }

    private static void SQLExceptionHandler (SQLException e, int i)
    {
        // handle any errors
        System.out.println("Point "+i);
        System.out.println("SQLException: " + e.getMessage());
        System.out.println("SQLState: " + e.getSQLState());
        System.out.println("VendorError: " + e.getErrorCode());
    }

}