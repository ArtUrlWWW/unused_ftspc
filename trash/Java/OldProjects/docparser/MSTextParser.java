import java.io.File;
import java.io.FileInputStream;
//import org.apache.poi.POIDocument;
//import org.apache.poi.POITextExtractor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.hssf.extractor.ExcelExtractor;
import org.apache.poi.hdgf.extractor.VisioTextExtractor;
import org.apache.poi.hslf.extractor.PowerPointExtractor;

public class MSTextParser implements Runnable
{
    
    public boolean busy = true;
    private boolean run = false;
    public int myid = -1;
    public String id = "";
    public String fileName = null;
    public boolean upd = false;
    private Thread th = null;
    
    public MSTextParser(int i)
    {
        myid = i;
        busy = false;
        run  = true;
        th = new Thread(this);
        th.start();
    }
    
    protected void startApp() {
        run = true;
    }

    protected void destroyApp(boolean flag) {
        run = false;
    }

    public void run()
    {
        while (run)
        {
            if (busy & fileName != null)
            {
                String s = null;
                MSHandler.fixing(id);

                try
                {
                    s = docToText();
                }
                catch(Throwable e)
                {
                    System.out.println("A problem occured while parsed MS document " + fileName + "(id = " + id +") "+e.getMessage());
                }                
                MSHandler.save(id, s, upd);                
                fileName = null;
                s = null;
                busy = false;
                try
                {
                    System.gc();
                }
                catch(Throwable e1)
                {
                    System.out.println("A problem occured while GC running " + fileName + "(id = " + id +") "+e1.getMessage());
                }
            }      
            else
            {
                try
                {
                    th.sleep(50);
                }
                catch(Throwable e)
                {
                }
            }
        }
    }

    private String docToText() throws Exception
    {
        String res = "";
        File f = new File(fileName);  
        if (!f.isFile())
        {
            System.out.println("File " + fileName + " does not exist.");
        }
        else
        {
//                res = POITextExtractor(new POIDocument(new POIFSFileSystem(new FileInputStream(f)))).getText();
//            try
//            {
                String ext = fileName.substring(fileName.length()-3).toUpperCase();
                if      (ext.equals("DOC")) res = (new WordExtractor(new FileInputStream(f)).getText());
                else if (ext.equals("XLS")) res = (new ExcelExtractor(new POIFSFileSystem(new FileInputStream(f))).getText());
                else if (ext.equals("PPT") || ext.equals("PPS"))  res = (new PowerPointExtractor(new FileInputStream(f)).getText());
                else res = (new VisioTextExtractor(new FileInputStream(f)).getText());
//            }
//            catch (Exception e) 
//            {
//                System.out.println("Document parsing error: "+e.getMessage());
//            }
        }
        return res;
    }

}