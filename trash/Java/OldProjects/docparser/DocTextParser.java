import java.io.File;
import java.io.FileInputStream;
import org.apache.poi.hwpf.extractor.WordExtractor;

public class DocTextParser implements Runnable
{
    
    public boolean busy = true;
    private boolean run = false;
    public int myid = -1;
    public int id = 0;
    public String fileName = null;
    public boolean upd = false;
    private Thread th = null;
    
    public DocTextParser(int i)
    {
        myid = i;
        busy = false;
        run  = true;
        th = new Thread(this);
        th.start();
    }
    
    protected void startApp() {
        run = true;
    }

    protected void destroyApp(boolean flag) {
        run = false;
    }

    public void run()
    {
        while (run)
        {
            if (busy & fileName != null)
            {
                String s = null;
                DocHandler.fixing(id);
                try
                {
                    s = docToText();
                }
                catch(Throwable e)
                {
                    System.out.println("A problem occured while parsed DOC " + fileName + "(id = " + id +") "+e.getMessage());
                }                
                DocHandler.save(id, s, upd);                
                fileName = null;
                s = null;
                busy = false;
                try
                {
                    System.gc();
                }
                catch(Throwable e1)
                {
                    System.out.println("A problem occured while GC running " + fileName + "(id = " + id +") "+e1.getMessage());
                }
            }      
            else
            {
                try
                {
                    th.sleep(50);
                }
                catch(Throwable e)
                {
                }
            }
        }
    }

    private String docToText() throws Exception
    {
        String res = "";
        File f = new File(fileName);  
        if (!f.isFile())
        {
            System.out.println("File " + fileName + " does not exist.");
        }
        else
        {
//            try
//            {
                res = (new WordExtractor(new FileInputStream(f)).getText());
//            }
//            catch (Exception e) 
//            {
//                System.out.println("Document parsing error: "+e.getMessage());
//            }
        }
        return res;
    }

}