/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mysql_lucene;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.analysis.snowball.SnowballAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.morphology.russian.RussianAnalayzer;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Searcher;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;

/**
 *
 * @author AKhusnutdinov
 */
public class search_test extends Thread {

    RAMDirectory idx = null;
    //RussianAnalayzer analyzer=null;
    SnowballAnalyzer analyzer = new SnowballAnalyzer(Version.LUCENE_CURRENT, "English");
    //SnowballAnalyzer analyzer = new SnowballAnalyzer(Version.LUCENE_CURRENT, "Russian");
    //StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_CURRENT);
    String field = "name";
    String string_for_search="";

    public void run(RAMDirectory idx, RussianAnalayzer analyzer, String string_for_search) {
        this.idx = idx;
        this.string_for_search=string_for_search;
        //this.analyzer=analyzer;
        this.start();
    }

    @Override
    public void run() {
        Date start = new Date();
        Date start1 = null;
        Date start2 = null;
        Date start4 = null;
        Date start5 = null;
        try {

            boolean raw = false;

            int hitsPerPage = 500;
            String line = string_for_search;
            //String line = "Natural language understanding";
            //String line = "The quick brown fox jumped over the lazy dogs";

            //IndexReader reader = IndexReader.open(idx, true); // only searching, so read-only=true
            IndexReader reader = IndexReader.open(FSDirectory.open(new File("c:/temp/index")), true); // only searching, so read-only=true

            //Searcher searcher = new IndexSearcher(idx);

            Searcher searcher = new IndexSearcher(reader);
            start1 = new Date();

            QueryParser parser = new QueryParser(Version.LUCENE_CURRENT, field, analyzer);

            start2 = new Date();
            line = line.trim();
            start4 = new Date();
            Query query = parser.parse(line);
            start5 = new Date();

            System.out.println("Searching for: " + query.toString(field));

            doPagingSearch(searcher, query, reader, hitsPerPage, raw, null == null);

            searcher.close();
        } catch (Exception ex) {
            Logger.getLogger(MySQL_LuceneView.class.getName()).log(Level.SEVERE, null, ex);
        }
        Date end = new Date();
        System.out.println("Time: " + (end.getTime() - start.getTime()) + "ms"
                + (end.getTime() - start1.getTime()) + "ms"
                + (end.getTime() - start2.getTime()) + "ms"
                + (end.getTime() - start4.getTime()) + "ms"
                + (end.getTime() - start5.getTime()) + "ms");
    }

    public void doPagingSearch(Searcher searcher, Query query,
            IndexReader reader, int hitsPerPage, boolean raw,
            boolean interactive) throws IOException {

        // Collect enough docs to show 5 pages
        TopScoreDocCollector collector = TopScoreDocCollector.create(
                5 * hitsPerPage, false);
        searcher.search(query, collector);
        ScoreDoc[] hits = collector.topDocs().scoreDocs;

        int numTotalHits = collector.getTotalHits();
        System.out.println(numTotalHits + " total matching documents");

        int start = 0;
        //int end = Math.min(numTotalHits, hitsPerPage);
        int end = 0;

        end = Math.min(hits.length, start + hitsPerPage);

        QueryScorer scorer = new QueryScorer(query, reader, "name");
        SimpleHTMLFormatter htmlFormatter = new SimpleHTMLFormatter("<font color=\"#FF0000\">", "</font>");
        Highlighter highlighter = new Highlighter(htmlFormatter, scorer);


        for (int i = start; i < end; i++) {
            try {
                /* if (raw) {                              // output raw format
                System.out.println("doc=" + hits[i].doc + " score=" + hits[i].score);
                continue;
                }*/
                Document doc = searcher.doc(hits[i].doc);
                System.out.println("   name: " + doc.get("name") + " score=" + hits[i].score);
                System.out.println("   fullpath: " + doc.get("fullpath"));
                /*Explanation expl = searcher.explain(query, hits[i].doc);
                String asText = expl.toString();
                System.out.println(asText);*/
                String fragment = highlighter.getBestFragment(analyzer, "name", doc.getField("name").stringValue());
                System.out.println(fragment);
                System.out.println("-----------------------");
            } catch (InvalidTokenOffsetsException ex) {
                Logger.getLogger(search_test.class.getName()).log(Level.SEVERE, null, ex);
            }


        }
    }
}
