/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package full_text_search;

import java.io.*;
import java.sql.*;
import java.util.*;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.KeywordAnalyzer;
import org.apache.lucene.analysis.SimpleAnalyzer;
import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

public class Main {

    private static Connection con;
    private static IndexWriter writer;
    /*public static String return_field = lucene_registry.get("return_field");
    public static String search_fields = lucene_registry.get("search_fields");
    public static String search_table = lucene_registry.get("search_table");
    public static String index_flag = lucene_registry.get("index_flag");*/
    public static String return_field = "id";
    public static String search_fields = "name,path,fullpath";
    //public static String search_fields = "name, fullpath";
    public static String search_table = "files";
    public static String index_flag = "indexed";

    public static void main(String[] args) throws Exception {
        // Loading configuration
//      lucene_registry.load(args[0]);

        //String dbhost = lucene_registry.get( "dbhost" );
        String dbhost = "localhost";
        //String dbuser = lucene_registry.get( "dbuser" );
        String dbuser = "searcher";
        //String dbpass = lucene_registry.get( "dbpass" );
        String dbpass = "searcher";
        //String dbname = lucene_registry.get( "dbname" );
        String dbname = "searcher";
        //String index_dir = lucene_registry.get( "index_dir" );




        System.out.println("!!!" + dbhost + " " + dbuser + " " + dbpass + " " + dbpass);
        // Open index
        File index = new File("C:/temp");
        Directory dir = FSDirectory.open(index);
        if (!index.exists()) {
            System.out.println("Index directory does not exist");
            System.exit(0);
        }
        //Directory index = new RAMDirectory();
        try {
            //writer = new IndexWriter(dir, new StandardAnalyzer(Version.LUCENE_30), true, IndexWriter.MaxFieldLength.LIMITED);
            writer = new IndexWriter(dir, new SimpleAnalyzer(), true, IndexWriter.MaxFieldLength.LIMITED);
        } catch (Exception ex) {
            System.out.print(ex.toString());
        }

        // Connecting to database
        String driver = "com.mysql.jdbc.Driver";
        String connection = "jdbc:mysql://" + dbhost + ":3306/" + dbname;
        Class.forName(driver);
        con = DriverManager.getConnection(connection, dbuser, dbpass);

        // Get unindexed documents from database
        Statement stmt = con.createStatement();
        String sql = "SELECT " + return_field + "," + search_fields + " FROM " + search_table + " WHERE " + index_flag + "=0";
        //String sql = "SELECT name, path, fullpath  FROM files WHERE indexed=0";
        ResultSet rs = stmt.executeQuery(sql);
        while (rs.next()) {
            indexDocument(rs);
        }
        con.close();
        rs.close();
        stmt.close();

        // Optimize index
        System.out.println("Optimizing index");
        writer.optimize();
        writer.close();
        System.out.println("Done");
    }

    public static void indexDocument(ResultSet rs) throws Exception {
        String temp1="";
        String temp2="";
        String temp3="";
        String temp4="";
        String[] search_fieldss = search_fields.split(",");
        Document doc = new Document();
        temp1="_" + return_field;
        temp2=rs.getString(return_field);
        doc.add(new Field(temp1, temp2, Field.Store.YES, Field.Index.ANALYZED));
        for (int i = 0; i < search_fieldss.length; i++) {
            temp3="_" + search_fieldss[i];
            temp4=rs.getString(search_fieldss[i]);
            doc.add(new Field(temp3, temp4, Field.Store.YES, Field.Index.ANALYZED_NO_NORMS));
        }
        writer.addDocument(doc);
        //System.out.println("Added to index post ID=" + rs.getString(return_field));
        Statement stmt = con.createStatement();
        stmt.executeUpdate("UPDATE " + search_table + " SET " + index_flag + "=1 WHERE " + return_field + "='" + rs.getString(return_field)+"'");
        //stmt.executeUpdate("UPDATE files SET indexed=1 WHERE fullpath='" + rs.getString("fullpath")+"'");
    }
}

class lucene_registry {

    private static Properties props = null;

    public static String get(String key) {
        if (props == null) {
            return "";
        }
        return props.getProperty(key);
    }

    public static void load(String filename) {
        try {
            if (props == null) {
                props = new Properties();
                props.load(new FileInputStream(filename));
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
