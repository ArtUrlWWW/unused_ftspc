import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.List;
import java.util.Properties;
import java.sql.Connection;
import java.sql.Statement;

public class Grabber {

    public static PDFLoader[] loader = null;
    public static int counter = 1;
    public static String dir = ".";
    public static String producerName = "";

    public static void main(String[] args) throws Exception {
        System.out.println("Started");
        Properties prop = new Properties();
        prop.load(new FileInputStream("Grabber.ini"));
        String sourceUrlString = prop.getProperty("URL");
        counter = Integer.parseInt(prop.getProperty("Threads"));
        dir = prop.getProperty("Outputpath");
        loader = new PDFLoader[counter];
        for (int i = 0; i < counter; i++) loader[i] = new PDFLoader(i);
        if (sourceUrlString.indexOf(':') == -1) sourceUrlString = "file:" + sourceUrlString;
        Source source = new Source(new URL(sourceUrlString));
        source.fullSequentialParse();
        List<Element> linkElements = source.getAllElements(HTMLElementName.A);
        source = null;
        for (Element linkElement : linkElements) {
            String href = linkElement.getAttributeValue("href");
            if (href == null) continue;
            if (href.startsWith("http://datasheets.ru/producers/")) {
                producerName = href.substring(31, href.length() - 5);
                try {
                    File f = new File(dir + "/" + producerName);
                    f.mkdir();
                }
                catch (Exception e) {
                }
                String producer = "http://datasheets.ru/datasheets/search/go/?search_mode=manufacturer&manufacturer=" + producerName;
                if (producer.indexOf(':') == -1) producer = "file:" + producer;
                pageGrabber(producer, 1);
            }
        }
        for (int k = 0; k < counter; k++) {
            if (loader[k] != null) {
                while (loader[k].busy)
                    try {
                        Thread.sleep(100);
                    }
                    catch (Exception e8) {
                    }
                loader[k].destroyApp(true);
            }
        }
        System.out.println("Finished");
        System.gc();
    }

    private static void pageGrabber(String producer, int pageno) throws Exception {
        Connection conn7 = null;
        Statement s7 = null;
        String s77 = "";

        //System.out.println(producer);
        Source source = new Source(new URL(producer));
        source.fullSequentialParse();
        List<Element> prodElements = source.getAllElements(HTMLElementName.A);
        source = null;
        String prev = "";
        for (Element prodElement : prodElements) {
            String pref = prodElement.getAttributeValue("href");
            if (pref == null) continue;
            if (pref.startsWith("http://datasheets.ru/datasheets/")) {
                if (pref.endsWith(".html") && !pref.equals(prev)) {

                    //++++++++++++++++++++++++++++++++++++++++++++
                    conn7 = PDFLoader.getConnection();
                    s7 = conn7.createStatement();

                    if (PDFLoader.countRows(conn7, "grabdatshets", pref) > 0) {
                        System.out.println("Uje skachali " + pref);
                    } else {
                        s77 = "insert into grabdatshets values('" + pref + "')";
                        int k777 = s7.executeUpdate(s77);
                        ///////////////////////////////////////////////
                        prev = pref;
                        boolean b = true;
                        while (b) {
                            int k = 0;
                            for (k = 0; k < counter; k++) {
                                b = loader[k].busy;
                                if (!b) {
                                    loader[k].busy = true;
                                    //System.out.println("Start " + pref + " by " + k);
                                    loader[k].dir = dir + "/" + producerName;
                                    loader[k].pref = pref;
                                    break;
                                }
                            }
                            if (k == counter)
                                try {
                                    Thread.sleep(100);
                                }
                                catch (Exception e8) {
                                }
                        }
                        ///////////////////////////////////////////////

                    }
                    s7.close();
                    conn7.close();

                    //++++++++++++++++++++++++++++++++++++++++++++

                } else {
                    if (pref.endsWith("startpage=" + (pageno + 1))) {
                        pageGrabber(pref, pageno + 1);
                        break;
                    }
                }
            }
        }
    }

}