import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;

import java.io.*;
import java.net.URL;
import java.sql.*;
import java.util.List;

public class PDFLoader implements Runnable {

    public boolean busy = true;
    private boolean run = false;
    public int myid = -1;
    public String pref = null;
    public String dir = null;
    public Thread th = null;

    public PDFLoader(int i) {
        myid = i;
        busy = false;
        run = true;
        th = new Thread(this);
        th.start();
    }

    protected void startApp() {
        run = true;
    }

    protected void destroyApp(boolean flag) {
        run = false;
    }

    public static Connection getConnection() throws Exception {
        String driver = "org.gjt.mm.mysql.Driver";
        String url = "jdbc:mysql://localhost/admin_dvig";
        String username = "parser";
        String password = "parser";

        Class.forName(driver); // load MySQL driver
        Connection conn = DriverManager.getConnection(url, username, password);
        return conn;
    }

    public static int countRows(Connection conn, String tableName, String flname) throws SQLException {
        // select the number of rows in the table
        Statement stmt = null;
        ResultSet rs = null;
        int rowCount = -1;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT COUNT(*) FROM " + tableName + " where filelink='" + flname + "'");
            // get the number of rows from the result set
            rs.next();
            rowCount = rs.getInt(1);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            rs.close();
            stmt.close();
        }
        return rowCount;
    }

    public void run() {
        String inptraf = "";
        String outptraf = "";
        String tempstr = "";
        String tempstr1 = "";
        double inptrafint = 0;
        double outptrafint = 0;
        int a = 0;
        int b = 0;
        int proverka = 1;
        Connection conn = null;
        Statement s2 = null;
        String s = "";

        while (run) {
            ///////////////////////      

            if (busy & pref != null) {


                //       Date today = Calendar.getInstance().getTime();

                // (2) create our "formatter"
                // SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-hh.mm.ss");

                // (3) create a new String in the format we want
                // String folderName = formatter.format(today);

                // (4) this prints "Folder Name = 2009-09-06-08.23.23"
                // System.out.println("Folder Name = " + folderName);

                while (proverka > 0) {

                    try {
                        String line;
                        //Process p1 = Runtime.getRuntime().exec("vnstat -u");
                        Process p = Runtime.getRuntime().exec("vnstat");
                        BufferedReader input =
                                new BufferedReader
                                        (new InputStreamReader(p.getInputStream()));
                        while ((line = input.readLine()) != null) {
                            if (line.indexOf("today") > 0) {
                                tempstr = "today    ";
                                a = line.indexOf(tempstr) + tempstr.length() + 2;
                                b = line.indexOf("|") - line.indexOf(tempstr) + 4;
                                inptraf = line.substring(a, b);
                                if (inptraf.indexOf("GB") > 1) {
                                    inptraf = inptraf.substring(0, inptraf.indexOf(" "));
                                    inptrafint = Double.parseDouble(inptraf) * 1024;
                                }

                                tempstr = " |   ";
                                a = line.indexOf(tempstr) + tempstr.length();
                                tempstr1 = line.substring(a);
                                a = tempstr1.indexOf(tempstr) + tempstr.length();
                                outptraf = tempstr1.substring(a);
                                if (outptraf.indexOf("GB") > 1) {
                                    outptraf = outptraf.substring(0, outptraf.indexOf(" "));
                                    outptrafint = Double.parseDouble(outptraf) * 1024;
                                }

                                if ((outptrafint / inptrafint) > 5) {
                                    proverka = 0;
                                } else {
                                    System.out.println("Limit 1/5 reached !!!");
                                    th.sleep((1000L) * 3600);
                                }
                            }
                        }
                        input.close();
                    }
                    catch (Exception err) {
                        err.printStackTrace();
                    }
                }
                proverka = 1;

                // today = Calendar.getInstance().getTime();

                // (2) create our "formatter"
                //formatter = new SimpleDateFormat("yyyy-MM-dd-hh.mm.ss");

                // (3) create a new String in the format we want
                // folderName = formatter.format(today);

                // (4) this prints "Folder Name = 2009-09-06-08.23.23"
                // System.out.println("Folder Name = " + folderName);

                int p = pref.lastIndexOf("/");
                String fname = pref.substring(p + 1, pref.lastIndexOf(".")) + ".pdf";
                if (p > -1) pref = pref.substring(0, p + 1) + "data-" + pref.substring(p + 1);
                //System.out.println("     " + pref);
                try {
                    Source source = new Source(new URL(pref));
                    source.fullSequentialParse();
                    List<Element> frameElements = source.getAllElements(HTMLElementName.IFRAME);
                    for (Element frameElement : frameElements) {
                        String prei = frameElement.getAttributeValue("src");
                        //System.out.println("          " + prei);
                        //////////////////////////////////////////
                        try {
                            conn = getConnection();
                            s2 = conn.createStatement();

                            if (countRows(conn, "grabdatshets", prei) > 0) {
                                System.out.println("Uje skachali " + prei);
                            } else {
                                s = "insert into grabdatshets values('" + prei + "')";
                                int k = s2.executeUpdate(s);
                                InputStream in = (new URL(prei)).openStream();
                                OutputStream out = new FileOutputStream(new File(dir, fname));
                                byte[] buf = new byte[1024];
                                int len;
                                while ((len = in.read(buf)) > 0) out.write(buf, 0, len);
                                in.close();
                                out.close();
                            }
                            s2.close();
                            conn.close();
                        }
                        catch (Exception qqq) {
                            qqq.printStackTrace();
                        }
                        //////////////////////////////////////////

                    }
                }
                catch (Exception e) {
                    System.out.println("A problem occured while loaded " + pref + " by parser " + myid + " " + e.getMessage());
                }


                pref = null;
                busy = false;
                try {
                    System.gc();
                }
                catch (Throwable e1) {
                    System.out.println("A problem occured while GC running " + pref + " by parser " + myid + " " + e1.getMessage());
                }
            } else {
                try {
                    th.sleep(50);
                }
                catch (Throwable e) {
                }
            }
            ////////////////
    
        }
    }

}