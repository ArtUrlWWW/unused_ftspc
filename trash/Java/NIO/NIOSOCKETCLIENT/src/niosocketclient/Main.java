/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package niosocketclient;

import java.io.FileOutputStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SocketChannel;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AKhusnutdinov
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {

            URL u = new URL("http://127.0.0.1");
            String host = u.getHost();
            int port = 2345;
            String file = "/";

            SocketAddress remote = new InetSocketAddress(host, port);
            SocketChannel channel = SocketChannel.open(remote);
            FileOutputStream out = new FileOutputStream("yourfile.htm");
            FileChannel localFile = out.getChannel();

            String request = "GET " + file + " HTTP/1.1\r\n" + "User-Agent: HTTPGrab\r\n"
                    + "Accept: text/*\r\n" + "Connection: close\r\n" + "Host: " + host + "\r\n" + "\r\n";
            request="GET";

            ByteBuffer header = ByteBuffer.wrap(request.getBytes("UTF-8"));
            channel.write(header);

            ByteBuffer buffer = ByteBuffer.allocate(777);
            int q=channel.read(buffer);
            while (q != -1) {

                buffer.flip();
                //localFile.write(buffer);
                System.out.println(q+"+++"+new String(buffer.array()).substring(0, q));
                buffer.clear();
                q=channel.read(buffer);
            }

            localFile.close();
            channel.close();

            
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
