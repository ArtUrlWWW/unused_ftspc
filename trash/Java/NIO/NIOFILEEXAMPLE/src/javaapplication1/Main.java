/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AKhusnutdinov
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            RandomAccessFile aFile = new RandomAccessFile("data/nio-data.txt", "rw");
            FileChannel inChannel = aFile.getChannel();
            ByteBuffer buf = ByteBuffer.allocate(48);
            byte qwe[] = new byte[777];
            int bytesRead = inChannel.read(buf);
            int x = 0;

            while (bytesRead != -1) {
                System.out.println("Read " + bytesRead);
                System.out.println(buf);
                buf.flip();

                while (buf.hasRemaining()) {
                    qwe[x] = buf.get();
                    System.out.println(qwe[x]);
                    x++;
                }
                buf.clear();
                bytesRead = inChannel.read(buf);
            }
            System.out.println(new String(qwe, "UTF-8"));

            aFile.close();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
