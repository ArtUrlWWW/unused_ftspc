/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nioserversocketexample;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/**
 *
 * @author AKhusnutdinov
 */
public class Main_old {

    public final static int PORT = 2345;

    public static void main(String[] args) throws Exception {

        ServerSocketChannel serverChannel = ServerSocketChannel.open();
        SocketAddress port = new InetSocketAddress(PORT);
        ByteBuffer buf = ByteBuffer.allocateDirect(1024);
        serverChannel.socket().bind(port);

        while (true) {
            SocketChannel clientChannel = serverChannel.accept();
            int numBytesRead = clientChannel.read(buf);

            if (numBytesRead == -1) {
                // No more bytes can be read from the channel
//                clientChannel.close();
                System.out.println("No Data to read");
            } else {
                // To read the bytes, flip the buffer
                buf.flip();
                while (buf.hasRemaining()) {
                    System.out.print((char) buf.get()); // read 1 byte at a time
                }
                buf.clear();

                // Read the bytes from the buffer ...;
                // see Getting Bytes from a ByteBuffer
            }

            String response = "Hello " + clientChannel.socket().getInetAddress() + " on port "
                    + clientChannel.socket().getPort() + "\r\n";
            response += "This is " + serverChannel.socket() + " on port "
                    + serverChannel.socket().getLocalPort() + "\r\n";

            byte[] data = response.getBytes("UTF-8");
            ByteBuffer buffer = ByteBuffer.wrap(data);
            while (buffer.hasRemaining()) {
                clientChannel.write(buffer);
            }
            clientChannel.close();
        }
    }
}
