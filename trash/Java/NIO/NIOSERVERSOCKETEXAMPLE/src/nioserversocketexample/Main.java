/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nioserversocketexample;

/**
 *
 * @author AKhusnutdinov
 */
public class Main {

    public final static int PORT = 2345;

    public static void main(String[] args) throws Exception {
        Socks4Proxy server = new Socks4Proxy();
        server.host = "127.0.0.1";
        server.port = 2345;
        server.run();
    }
}
