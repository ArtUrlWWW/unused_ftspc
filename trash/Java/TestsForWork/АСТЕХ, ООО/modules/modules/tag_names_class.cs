﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using System.Windows.Forms;

namespace modules
{
    public class tag_names_class
    {
        public static void Parameter_logic(XmlElement xmlnode_tmp, DataTable d, DataGridView dataGridView1)
        {
            XmlAttributeCollection xmlattrc_tmp = xmlnode_tmp.Attributes;
            d.Rows.Add();
            for (int x = 0; x < xmlattrc_tmp.Count; x++)
            {
                d.Columns.Add(xmlattrc_tmp[x].Name);
                d.Rows[0][x] = xmlattrc_tmp[x].Value;
                //info_txt += xmlattrc_tmp[x].Name + "='" + xmlattrc_tmp[x].Value + "' ";
            }
            dataGridView1.DataSource = d;
        }
    }
}
