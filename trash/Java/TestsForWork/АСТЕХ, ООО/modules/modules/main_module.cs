﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml;
using System.IO;
using System.Windows.Forms;

namespace modules
{
    public class modules_for_xml
    {
        public static void add_to_table(String string_for_tag, DataGridView dataGridView1)
        {
            List<string> _names = new List<string>();
            DataTable d = new DataTable();
            String node_name = "";

            dataGridView1.Columns.Clear();
            dataGridView1.AutoSize = true;

            XmlDocument doc_tmp = new XmlDocument();
            doc_tmp.Load(new StringReader(string_for_tag));
            XmlElement xmlnode_tmp = doc_tmp.DocumentElement;
            node_name = xmlnode_tmp.Name;
                       
            /*
            switch (node_name)
            {
                case "Parameter":
                    MessageBox.Show("Parameter");
                    tag_names_class.Parameter_logic(
                    break;
            }
            */

            tag_names_class.Parameter_logic(xmlnode_tmp, d, dataGridView1);
        }
    }
}
