﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace DLL_project
{
    public class functions_for_UI
    {
        public static Boolean save_path_to_the_registry(String folder_path)
        {
            try
            {
                RegistryKey HeadRK = Registry.CurrentUser;
                RegistryKey regname = HeadRK.CreateSubKey("XML_TEST");
                regname.SetValue("savepath", folder_path);
                return true;
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex);
                return false;
            }
        }

        public static String get_last_selected_path_from_the_registry()
        {
            try
            {
                RegistryKey HeadRK = Registry.CurrentUser;
                RegistryKey regname = HeadRK.CreateSubKey("XML_TEST");
                return regname.GetValue("savepath").ToString();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex);
                return "Error: " + ex.ToString();
            }
        }

        public static void load_xml_file(Object selected_file, String selectedfolder, System.Windows.Forms.TreeView treeView1)
        {
            int position1 = -1;
            int position2 = -1;
            String info_txt = "";
            String node_name = "";

            if (selected_file != null && MessageBox.Show("Загрузить данный файл: "
                    + selected_file + " ?", "Вопрос:",
                    MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                treeView1.BeginUpdate();
                treeView1.Nodes.Clear();

                XmlTextReader reader = new XmlTextReader(selectedfolder + "\\" + selected_file);
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:                            

                            if (reader.Depth == 0)
                            {
                                treeView1.Nodes.Add(reader.Name);
                                position1++;
                            }

                            if (reader.Depth == 1)
                            {
                                treeView1.Nodes[position1].Nodes.Add(reader.Name);
                                position2++;
                            }

                            if (reader.Depth == 2)
                            {
                                info_txt = "";
                                node_name = reader.Name;
                                
                                String string_for_tag=reader.ReadOuterXml();
                                XmlDocument doc_tmp = new XmlDocument();
                                doc_tmp.Load(new StringReader(string_for_tag));
                                XmlElement xmlnode_tmp = doc_tmp.DocumentElement;

                                XmlAttributeCollection xmlattrc_tmp = xmlnode_tmp.Attributes;

                                for (int x = 0; x < xmlattrc_tmp.Count; x++)
                                {
                                    info_txt += xmlattrc_tmp[x].Name + "='" + xmlattrc_tmp[x].Value + "' ";
                                }                               
                                treeView1.Nodes[position1].Nodes[position2].Nodes.Add(node_name + " " + info_txt).Tag = string_for_tag;                                
                            }                            
                            break;
                        case XmlNodeType.Text:
                            break;
                        case XmlNodeType.EndElement:
                            break;
                    }
                }
                treeView1.EndUpdate();
            }
        }
    }
}
