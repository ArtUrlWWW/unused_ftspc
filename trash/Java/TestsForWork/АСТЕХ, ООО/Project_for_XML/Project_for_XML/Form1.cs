﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DLL_project;
using System.IO;
using System.Xml;
using modules;

namespace Project_for_XML
{
    public partial class Form1 : Form
    {
        String selectedfolder = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void main_button(object sender, EventArgs e)
        {
            String last_selectedfolder = "";
            button3.Enabled = false;
            dataGridView1.AutoSize = true;
            button3.Text = "Демо-версия: можно выбирать лишь раз.";
            folderBrowserDialog1.ShowNewFolderButton = false;
            last_selectedfolder = functions_for_UI.get_last_selected_path_from_the_registry();
            label1.Text = "Выберите каталог";
            if (last_selectedfolder.IndexOf("Error:") > -1)
            {
                MessageBox.Show("Проблемы с чтением из реестра");
                last_selectedfolder = @"C:\";
            }
            folderBrowserDialog1.SelectedPath = last_selectedfolder;

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                selectedfolder = folderBrowserDialog1.SelectedPath;
                if (!functions_for_UI.save_path_to_the_registry(selectedfolder))
                {
                    MessageBox.Show("Проблемы с сохранением в реест");
                    Application.Exit();
                }
            }

            if (selectedfolder == "")
            {
                MessageBox.Show("Вы не выбрали необходимый каталог \r\n Программа завершает свою работу");
                Application.Exit();
            }

            label1.Text = "Выбранный каталог запомнен";

            DirectoryInfo selected_folder_info = new DirectoryInfo(selectedfolder);
            foreach (FileInfo fi in selected_folder_info.GetFiles("*.xml"))
            {
                listBox1.Items.Add(fi.Name);
            }
            label1.Text = "Выберите нужный файл";
        }

        private void listBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            Object selected_file = listBox1.SelectedItem;
            functions_for_UI.load_xml_file(selected_file, selectedfolder, treeView1);
        }        

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (treeView1.SelectedNode != null && treeView1.SelectedNode.Tag != null)
            {                
                String string_for_tag = treeView1.SelectedNode.Tag.ToString();
                modules_for_xml.add_to_table(string_for_tag, dataGridView1);
            }
        }
    }
}