/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50148
Source Host           : localhost:3306
Source Database       : enterprises

Target Server Type    : MYSQL
Target Server Version : 50148
File Encoding         : 65001

Date: 2011-03-13 15:31:28
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `firm_list`
-- ----------------------------
DROP TABLE IF EXISTS `firm_list`;
CREATE TABLE `firm_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firmType` int(255) DEFAULT NULL,
  `code` varchar(5) NOT NULL,
  `name` varchar(25) NOT NULL,
  `comment` varchar(75) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of firm_list
-- ----------------------------
INSERT INTO `firm_list` VALUES ('1', '4', '111', 'FirmName1', 'Comment1');
INSERT INTO `firm_list` VALUES ('2', '4', '222', 'FirmName2', 'Comment2');
INSERT INTO `firm_list` VALUES ('3', '1', '333', 'FirmName3', 'Comment2');
INSERT INTO `firm_list` VALUES ('4', '3', '444', 'FirmName4', 'Comment4');

-- ----------------------------
-- Table structure for `firm_types`
-- ----------------------------
DROP TABLE IF EXISTS `firm_types`;
CREATE TABLE `firm_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(777) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=779 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of firm_types
-- ----------------------------
INSERT INTO `firm_types` VALUES ('1', 'UNKNOWN (DELETED)');
INSERT INTO `firm_types` VALUES ('2', 'x');
INSERT INTO `firm_types` VALUES ('3', 'w');
INSERT INTO `firm_types` VALUES ('4', 'e');
INSERT INTO `firm_types` VALUES ('5', 'r');
INSERT INTO `firm_types` VALUES ('6', 't');
INSERT INTO `firm_types` VALUES ('7', 'y');
