/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package khusnutdinov.arthur.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import khusnutdinov.arthur.HibernateFiles.FirmList;
import khusnutdinov.arthur.HibernateFiles.FirmTypes;
import khusnutdinov.arthur.HibernateFiles.NewHibernateUtil;
import khusnutdinov.arthur.vars.ClassOfVars;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author AKhusnutdinov
 */
public class Edit extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        ArrayList firmTypesList = new ArrayList();
        ArrayList firmsList = new ArrayList();
        ArrayList tmpfirmTypesList = new ArrayList();
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        VelocityContext context = new VelocityContext();
        VelocityEngine engine = new VelocityEngine();
        StringWriter writer = new StringWriter();
        Query q;
        Template t;
        List resultList;

        if (ClassOfVars.rootPathOfApplication.equals("")) {
            ServletConfig sg = getServletConfig();
            ServletContext sc = sg.getServletContext();
            ClassOfVars.rootPathOfApplication = sc.getRealPath("/");
        }

        try {
            session.beginTransaction();
            ClassOfVars.loadProps();
            engine.init(ClassOfVars.globalProps);
            t = engine.getTemplate("Edit.html", "UTF-8");

            if (request.getParameter("searchtypename") == null
                    || request.getParameter("searchtypename").equals("")) {
                q = session.createQuery("from FirmTypes");
            } else {
                q = session.createQuery("from FirmTypes where type like '%"
                        + request.getParameter("searchtypename") + "%'");
            }

            resultList = q.list();

            for (Object o : resultList) {
                FirmTypes firmtype = (FirmTypes) o;
                tmpfirmTypesList = new ArrayList();
                tmpfirmTypesList.add(firmtype.getId());
                tmpfirmTypesList.add(firmtype.getType());
                firmTypesList.add(tmpfirmTypesList);
            }

            context.put("firmTypesList", firmTypesList);

            if (request.getParameter("typeid") == null
                    || request.getParameter("typeid").equals("")) {
                q = session.createQuery("from FirmList");
            } else {
                q = session.createQuery("from FirmList where firmType="
                        + request.getParameter("typeid"));
            }
            resultList = q.list();

            for (Object o : resultList) {
                FirmList firm = (FirmList) o;
                tmpfirmTypesList = new ArrayList();
                tmpfirmTypesList.add(firm.getId());
                tmpfirmTypesList.add(firm.getFirmType().getType());
                tmpfirmTypesList.add(firm.getName());
                tmpfirmTypesList.add(firm.getCode());
                tmpfirmTypesList.add(firm.getComment());
                firmsList.add(tmpfirmTypesList);
            }
            context.put("firmsList", firmsList);


            t.merge(context, writer);
            out.println(writer.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
