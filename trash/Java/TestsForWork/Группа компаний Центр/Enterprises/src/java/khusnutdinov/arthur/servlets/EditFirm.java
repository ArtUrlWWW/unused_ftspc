package khusnutdinov.arthur.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import khusnutdinov.arthur.HibernateFiles.FirmList;
import khusnutdinov.arthur.HibernateFiles.FirmTypes;
import khusnutdinov.arthur.HibernateFiles.NewHibernateUtil;
import khusnutdinov.arthur.vars.ClassOfVars;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author KAN
 */
public class EditFirm extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        VelocityContext context = new VelocityContext();
        VelocityEngine engine = new VelocityEngine();
        StringWriter writer = new StringWriter();
        Template pageTemplate;
        Transaction tx = null;
        Query q;
        Query query;
        List resultList;
        ArrayList firmsList = new ArrayList();
        ArrayList firmTypesList = new ArrayList();
        ArrayList tmpfirmTypesList = new ArrayList();
        FirmList firm = null;
        FirmTypes firmtype = null;

        tx = session.beginTransaction();

        if (ClassOfVars.rootPathOfApplication.equals("")) {
            ServletConfig sg = getServletConfig();
            ServletContext sc = sg.getServletContext();
            ClassOfVars.rootPathOfApplication = sc.getRealPath("/");
        }

        try {
            ClassOfVars.loadProps();
            engine.init(ClassOfVars.globalProps);
            pageTemplate = engine.getTemplate("EditFirm.html", "UTF-8");

            if (request.getParameter("id") != null) {
                q = session.createQuery("from FirmList where id="
                        + request.getParameter("id"));
                resultList = q.list();

                for (Object o : resultList) {
                    firm = (FirmList) o;
                    firmsList = new ArrayList();
                    firmsList.add(firm.getId());
                    firmsList.add(firm.getFirmType().getId());
                    firmsList.add(firm.getCode());
                    firmsList.add(firm.getName());
                    firmsList.add(firm.getComment());
                }

                context.put("firmsList", firmsList);

                q = session.createQuery("from FirmTypes");
                resultList = q.list();

                for (Object o : resultList) {
                    firmtype = (FirmTypes) o;
                    tmpfirmTypesList = new ArrayList();
                    tmpfirmTypesList.add(firmtype.getId());
                    tmpfirmTypesList.add(firmtype.getType());
                    firmTypesList.add(tmpfirmTypesList);
                }

                context.put("firmTypesList", firmTypesList);

                if (request.getParameter("newCode") != null) {
                    if (request.getParameter("newComment").length() > 75) {
                        context.put("saved", "2");
                        context.put("backlink", "EditFirm.html?id="
                                + request.getParameter("id"));
                    } else {
                        context.put("saved", "1");
                        context.put("backlink", "Edit.html");
                        q = session.createQuery("from FirmTypes where id="
                                + request.getParameter("firmTypesList"));
                        resultList = q.list();
                        for (Object o : resultList) {
                            firmtype = (FirmTypes) o;
                        }
                        firm.setFirmType(firmtype);
                        firm.setCode(request.getParameter("newCode"));
                        firm.setName(request.getParameter("newName"));
                        firm.setComment(request.getParameter("newComment"));
                        session.update(firm);
                        tx.commit();
                    }
                } else {
                    context.put("saved", "0");
                }
            } else {
                out.println("Error");
            }

            if (request.getParameter("delitem") != null) {
                q = session.createQuery("from FirmList where id="
                        + request.getParameter("delitem"));
                resultList = q.list();
                for (Object o : resultList) {
                    firm = (FirmList) o;
                }
                context.put("saved", "1");
                session.delete(firm);
                tx.commit();
            }

            pageTemplate.merge(context, writer);
            out.println(writer.toString());

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
