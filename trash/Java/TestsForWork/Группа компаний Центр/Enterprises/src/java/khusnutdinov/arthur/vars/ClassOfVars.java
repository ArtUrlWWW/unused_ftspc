/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package khusnutdinov.arthur.vars;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author AKhusnutdinov
 */
public class ClassOfVars {

    public static String rootPathOfApplication = "";
    public static Properties globalProps = null;

    public static void loadProps() throws Exception {
        if (globalProps == null) {
            InputStream propertiesIn =
                    ClassOfVars.class.getResourceAsStream("velocity.properties");
            if (propertiesIn == null) {
                throw new FileNotFoundException("${archive}/khusnutdinov.arthur.vars/velocity.properties");
            }
            Properties properties = new Properties();
            properties.load(propertiesIn);
            properties.setProperty("file.resource.loader.path",
                    rootPathOfApplication + "/WEB-INF/classes/khusnutdinov/arthur/");
            globalProps = properties;
        }
    }
}
