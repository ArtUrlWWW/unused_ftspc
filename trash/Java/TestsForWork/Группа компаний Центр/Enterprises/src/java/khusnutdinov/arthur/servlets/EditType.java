package khusnutdinov.arthur.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import khusnutdinov.arthur.HibernateFiles.FirmTypes;
import khusnutdinov.arthur.HibernateFiles.NewHibernateUtil;
import khusnutdinov.arthur.vars.ClassOfVars;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author KAN
 */
public class EditType extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        VelocityContext context = new VelocityContext();
        VelocityEngine engine = new VelocityEngine();
        StringWriter writer = new StringWriter();
        Template pageTemplate;
        Transaction tx = null;
        Query q;
        Query query;
        List resultList;
        ArrayList firmTypesList = new ArrayList();
        FirmTypes firmtype = null;
        String hql = "update firm_list set firmType = :newFirmType where firmType = :oldFirmType";
        int rowCount;

        tx = session.beginTransaction();

        if (ClassOfVars.rootPathOfApplication.equals("")) {
            ServletConfig sg = getServletConfig();
            ServletContext sc = sg.getServletContext();
            ClassOfVars.rootPathOfApplication = sc.getRealPath("/");
        }

        try {
            ClassOfVars.loadProps();
            engine.init(ClassOfVars.globalProps);
            pageTemplate = engine.getTemplate("EditType.html", "UTF-8");

            if (request.getParameter("id") != null) {
                q = session.createQuery("from FirmTypes where id="
                        + request.getParameter("id"));
                resultList = q.list();

                for (Object o : resultList) {
                    firmtype = (FirmTypes) o;
                    firmTypesList = new ArrayList();
                    firmTypesList.add(firmtype.getId());
                    firmTypesList.add(firmtype.getType());
                }

                context.put("firmTypesList", firmTypesList);

                if (request.getParameter("newTypeName") != null) {
                    context.put("saved", true);
                    firmtype.setType(request.getParameter("newTypeName"));
                    session.update(firmtype);
                    tx.commit();
                } else {
                    context.put("saved", false);
                }
            } else {
                out.println("Error");
            }

            if (request.getParameter("delitem") != null) {
                q = session.createQuery("from FirmTypes where id="
                        + request.getParameter("delitem"));
                resultList = q.list();
                for (Object o : resultList) {
                    firmtype = (FirmTypes) o;
                }
                context.put("saved", true);
                session.delete(firmtype);
                tx.commit();

                query = session.createSQLQuery(hql);
                query.setString("oldFirmType", request.getParameter("delitem"));
                query.setString("newFirmType", "1");
                rowCount = query.executeUpdate();
                System.out.println("Rows affected: " + rowCount);
            }

            pageTemplate.merge(context, writer);
            out.println(writer.toString());

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
