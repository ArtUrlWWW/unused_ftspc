package khusnutdinov.arthur.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import khusnutdinov.arthur.HibernateFiles.FirmTypes;
import khusnutdinov.arthur.HibernateFiles.NewHibernateUtil;
import khusnutdinov.arthur.vars.ClassOfVars;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author KAN
 */
public class AddType extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        VelocityContext context = new VelocityContext();
        VelocityEngine engine = new VelocityEngine();
        StringWriter writer = new StringWriter();
        Template pageTemplate;
        Transaction tx = null;
        tx = session.beginTransaction();

        if (ClassOfVars.rootPathOfApplication.equals("")) {
            ServletConfig sg = getServletConfig();
            ServletContext sc = sg.getServletContext();
            ClassOfVars.rootPathOfApplication = sc.getRealPath("/");
        }

        try {
            ClassOfVars.loadProps();
            engine.init(ClassOfVars.globalProps);
            pageTemplate = engine.getTemplate("AddType.html", "UTF-8");

            if (request.getParameter("newTypeName") != null) {
                context.put("saved", true);
                FirmTypes newType = new FirmTypes();
                newType.setType(request.getParameter("newTypeName"));
                session.save(newType);
                tx.commit();
            } else {
                context.put("saved", false);
            }

            pageTemplate.merge(context, writer);
            out.println(writer.toString());

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
