package finddublicatedfiles.FileSystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;

/**
 *
 * @author KAN
 */
public class GetMD5File {

    public String count(String pathToFile) {
        String output = "";
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            File f = new File(pathToFile);
            InputStream is = new FileInputStream(f);
            byte[] buffer = new byte[819200];
            int read = 0;
            while ((read = is.read(buffer)) > 0) {
                digest.update(buffer, 0, read);
            }
            byte[] md5sum = digest.digest();
            BigInteger bigInt = new BigInteger(1, md5sum);
            output = bigInt.toString(16);
            is.close();
            buffer=null;
            md5sum=null;
            bigInt=null;
            f=null;            
            is=null;            
            digest=null;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }
}