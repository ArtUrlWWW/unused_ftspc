package finddublicatedfiles.FileSystem;

import finddublicatedfiles.FindDublicatedFiles;
import finddublicatedfiles.ThreadInserter;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author KAN
 */
public class Recursive {

    Connection c;
    String query;

    public Recursive() {
        // Загружаем класс драйвера
        try {
            Class.forName("org.hsqldb.jdbcDriver");
        } catch (ClassNotFoundException e) {
            System.err.println("НЕ удалось загрузить драйвер ДБ.");
            e.printStackTrace();
            System.exit(1);
        }

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(
                    "jdbc:hsqldb:file:z:/DB/TestDB", "SA", "");
        } catch (SQLException e) {
            System.err.println("НЕ удалось оздать соединение.");
            e.printStackTrace();
            System.exit(1);
        }

        try {
            FindDublicatedFiles.statement = connection.createStatement();
            // создаем таблицу со столбцами id и value.
            try {
                query = "CREATE TABLE fileHashes (id IDENTITY , pathToFile VARCHAR(777), MD5File VARCHAR(777))";
                FindDublicatedFiles.statement.executeUpdate(query);
                query = "CREATE INDEX id_idx on fileHashes (id)";
                FindDublicatedFiles.statement.executeUpdate(query);
                query = "CREATE INDEX pathToFile_idx on fileHashes (pathToFile)";
                FindDublicatedFiles.statement.executeUpdate(query);
                query = "CREATE INDEX MD5File_idx on fileHashes (MD5File)";
                FindDublicatedFiles.statement.executeUpdate(query);
            } catch (SQLException e) {
                //e.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void index(String path) {
        File main_dir = new File(path);
        File[] filesInDir = main_dir.listFiles();
        for (File currentFile : filesInDir) {
            if (currentFile.isDirectory()) {
                index(currentFile.getAbsolutePath());
            } else if (currentFile.isFile()) {
                try {
                    while (FindDublicatedFiles.threadsCount > 50) {
                        synchronized (this) {
                            wait(10);
                        }
                    }
                    FindDublicatedFiles.threadsCount++;
                    FindDublicatedFiles.filesCount++;
                    FindDublicatedFiles.filesSize+=currentFile.length();
                    ThreadInserter ThreadInserter = new ThreadInserter();
                    ThreadInserter.start_th(currentFile);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
