package finddublicatedfiles;

import finddublicatedfiles.FileSystem.GetMD5File;
import java.io.File;

/**
 *
 * @author KAN
 */
public class ThreadInserter extends Thread {

    String pathToFile;
    File currentFile;
    String query;

    public void start_th(File currentFile) {
        this.currentFile = currentFile;
        this.start();
    }

    @Override
    public void run() {
        try {
            GetMD5File GetMD5File = new GetMD5File();
            String MD5 = "";
            pathToFile = currentFile.getAbsolutePath();
            MD5 = GetMD5File.count(pathToFile);
            query = "INSERT INTO fileHashes (pathToFile, MD5File) VALUES('" + pathToFile.replace("'", "\"") + "', '" + MD5 + "')";
            FindDublicatedFiles.statement.executeUpdate(query);
            FindDublicatedFiles.threadsCount--;
        } catch (Exception ex) {
            System.out.println(query);
            FindDublicatedFiles.threadsCount--;
            ex.printStackTrace();
        }
    }
}
