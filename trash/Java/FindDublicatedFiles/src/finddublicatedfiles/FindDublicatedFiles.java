package finddublicatedfiles;

import finddublicatedfiles.FileSystem.Recursive;
import java.io.File;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author KAN
 */
public class FindDublicatedFiles {

    public static Statement statement;
    public static Integer threadsCount = 0;
    public static Integer filesCount = 0;
    public static Double filesSize = 0.0;
    public static Date startTime;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        FindDublicatedFiles FindDublicatedFiles = new FindDublicatedFiles();
        startTime = new Date();
        FindDublicatedFiles.search(0);
    }

    public void search(int x) {
        try {
            String main_path = "z:/Unsorted/";
            Timer timer1 = new Timer();
            Recursive Recursive = new Recursive();

            if (x > 0) {
                timer1.schedule(new Monitor(), 0, 5000);
                Recursive.index(main_path);

                while (FindDublicatedFiles.threadsCount > 0) {
                    try {
                        synchronized (this) {
                            wait(100);
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(FindDublicatedFiles.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            timer1.cancel();

            String query = "SELECT pathToFile, MD5File from ("
                    + "SELECT pathToFile, MD5File "
                    + "FROM fileHashes "
                    + "where MD5File in ("
                    + "SELECT MD5File "
                    + "FROM fileHashes "
                    + "GROUP BY MD5File "
                    + "HAVING COUNT(MD5File)>1) )"
                    + "ORDER BY MD5File";
//            String query = "SELECT pathToFile, MD5File "                 
//                    + "FROM fileHashes ";

            ResultSet resultSet = statement.executeQuery(query);

            // распечатываю
            while (resultSet.next()) {
                File testFile = new File(resultSet.getString(1));
                if (testFile.exists()) {
                    System.out.println(resultSet.getString(1)
                            + " | " + resultSet.getString(2));
                } else {
                    query ="";
                    query = "delete from fileHashes where pathToFile like '" + resultSet.getString(1).replace("'", "\'") + "'";
                    System.out.println(query);
                    FindDublicatedFiles.statement.executeUpdate(query);
                }
            }

            statement.execute("SHUTDOWN");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
