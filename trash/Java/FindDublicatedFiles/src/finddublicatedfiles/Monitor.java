package finddublicatedfiles;

import java.util.Date;
import java.util.TimerTask;

/**
 *
 * @author KAN
 */
public class Monitor extends TimerTask {

    @Override
    public void run() {
        try {
        Date currentTime = new Date();
        Long timeDifference = currentTime.getTime() - FindDublicatedFiles.startTime.getTime();
        timeDifference = timeDifference / 1000;
        Double speed;
        speed = ((FindDublicatedFiles.filesSize / (1024 * 1024)) / timeDifference);
        String speedStr = String.valueOf(speed);
        speedStr = speedStr.substring(0, speedStr.indexOf(".") + 3);
        System.out.println("Speed: " + speedStr + " MB/s");
        System.out.println("Кол-во потоков: " + FindDublicatedFiles.threadsCount);
        System.out.println("Обработано файлов: " + FindDublicatedFiles.filesCount);
        System.out.println("Общий размер: " + FindDublicatedFiles.filesSize);
        System.out.println("...................................................");
        } catch (Exception ex) {
        ex.printStackTrace();
        }
    }
}
