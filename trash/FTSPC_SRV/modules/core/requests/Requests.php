<?php

//ф
/**
 * Модель для обработки запросов.
 */
class Requests {

    function __construct() {
	// Находим папку, из в которой лежит скрипт
	$curDir = substr(StartUpController::$curDir, strrpos(StartUpController::$curDir, "/") + 1);

	// Убираем не нужные данные
	$this->valueFromURL = str_replace("?" . $_SERVER["QUERY_STRING"], '', $_SERVER['REQUEST_URI']);
	$this->valueFromURL =str_replace('/'.$curDir.'/index.php/', '', $this->valueFromURL);
	// Убираем слэши справа и слева строки
	$this->valueFromURL = preg_replace('/(^\/|\/$)/', '', $this->valueFromURL);
	// Преобразуем в массив
	$this->urlarray = explode('/', $this->valueFromURL);
    }

    /**
     * Преобразуем массив, созданный в конструкторе, в переменные, по шаблону,
     * переданному пользователем.
     * @param type $userPattern 
     */
    function get_vars($userPattern) {
	$userPatternArray = explode('/', $userPattern);
	foreach ($userPatternArray as $keyY => $valueE) {
	    if (@$this->urlarray[$keyY] != '') {
		//создаём внутри данного класса необходимые переменные
		$this->$valueE = $this->urlarray[$keyY];
	    } else {
		$this->$valueE = '';
	    }
	}
    }

}

?>
