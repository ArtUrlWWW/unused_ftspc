<?php

//ф
class StartUpController {

    static $request;
    static $curDir;

    /**
     * Загрузка конфигурационных данных
     */
    function LoadConfig($curDir) {
	$curDir = str_replace("\\", "/", $curDir);
	self::$curDir = $curDir;

	require_once MODULES_DIR . 'config/MainConfig.php';
	require_once MODULES_DIR . 'core/Data.php';
	require_once MODULES_DIR . 'core/requests/Requests.php';

	self::$request = new Requests();
    }

    function GetIp() {
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	    $ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
	    $ip = $_SERVER['REMOTE_ADDR'];
	}
	return $ip;
    }

}

?>
