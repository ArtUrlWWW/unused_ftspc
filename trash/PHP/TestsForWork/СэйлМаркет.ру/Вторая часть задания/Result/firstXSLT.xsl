<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
			<xsl:apply-templates select="//user"/>
		</root>
	</xsl:template>
	<xsl:template match="user">
		<user>
				<xsl:call-template name="catwalk">
					<xsl:with-param name="id">
						<xsl:value-of select="id"/>
					</xsl:with-param>
				</xsl:call-template>
		</user>
	</xsl:template>
	<xsl:template name="catwalk">
		<xsl:param name="id"/>
		<xsl:value-of select="//user[id = $id]/fname"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//user[id = $id]/sname"/>
		<xsl:text> / Привязанные категории: </xsl:text>
		<xsl:if test="$id != '0'">
			<xsl:for-each select="//link[id_user = $id]">
				<xsl:call-template name="getCategoryTitle">
					<xsl:with-param name="id">
						<xsl:value-of select="id_usertype"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<xsl:template name="getCategoryTitle">
		<xsl:param name="id"/>
		<xsl:value-of select="//user[id = $id]/id"/>
		<xsl:value-of select="//usertype[id = $id]/title"/>
		<xsl:text>, </xsl:text>
	</xsl:template>
</xsl:stylesheet>
