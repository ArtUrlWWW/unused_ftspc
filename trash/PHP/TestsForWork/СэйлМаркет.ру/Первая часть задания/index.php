<?php
error_reporting(E_ALL);
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>

        <?php
        $XML = new DOMDocument();
        $XML->load('secondXML.xml');

        $xslt = new XSLTProcessor();

        $XSL = new DOMDocument();
        $XSL->load('secondXSLT.xslt');
        $xslt->importStylesheet($XSL);
//        print $xslt->transformToXML($XML);

        $XML->loadXML($xslt->transformToXML($XML));

        $users = $XML->getElementsByTagName('usertype');

        foreach ($users as $user) {
            $node = $user->nodeValue;
            $subusers = $user->childNodes;
            $x = 0;
            foreach ($subusers as $subuser) {
                $node = $subuser->nodeValue;
                if ($x < 1) {
                    echo $node . " \r\n <br />";
                    $x++;
                } else {
                    echo "---" . $node . " \r\n <br />";
                }
            }
            echo "----------------------------- \r\n <br />";
        }
        ?>

    </body>
</html>
