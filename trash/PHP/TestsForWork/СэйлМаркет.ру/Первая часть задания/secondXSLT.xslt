<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
			<xsl:apply-templates select="//usertype[id_parent='0']">
				<xsl:with-param name="secondReq">0</xsl:with-param>
			</xsl:apply-templates>
		</root>
	</xsl:template>
	<xsl:template match="usertype">
		<xsl:param name="secondReq"/>
		<xsl:if test="$secondReq = 0">
			<usertype>
				<xsl:variable name="id" select="id"/>
				<xsl:value-of select="//usertype[id = $id]/title"/>
				<xsl:text> ID: </xsl:text>
				<xsl:value-of select="id"/>
				<xsl:text> Parent ID: </xsl:text>
				<xsl:value-of select="//usertype[id = $id]/id_parent"/>
				<xsl:apply-templates select="//usertype[id_parent=$id]">
					<xsl:with-param name="secondReq">
						1
					</xsl:with-param>
				</xsl:apply-templates>
			</usertype>
		</xsl:if>
		<xsl:if test="$secondReq = 1">
			<usersubtype>
				<xsl:variable name="id" select="id"/>
				<xsl:value-of select="//usertype[id = $id]/title"/>
				<xsl:text> ID: </xsl:text>
				<xsl:value-of select="id"/>
				<xsl:text> Parent ID: </xsl:text>
				<xsl:value-of select="//usertype[id = $id]/id_parent"/>
				<xsl:apply-templates select="//usertype[id_parent=$id]">
					<xsl:with-param name="secondReq">
						1
					</xsl:with-param>
				</xsl:apply-templates>
			</usersubtype>
		</xsl:if>
	</xsl:template>
	<xsl:template name="recurfunct1">		
		<xsl:param name="id"/>
	</xsl:template>
	<xsl:template name="recurfunct">
		<xsl:param name="id_parent"/>
		<xsl:param name="id"/>
	</xsl:template>
</xsl:stylesheet>
