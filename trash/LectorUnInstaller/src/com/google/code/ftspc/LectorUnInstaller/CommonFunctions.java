/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.google.code.ftspc.LectorUnInstaller;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/**
 *
 * @author AKhusnutdinov
 */
public class CommonFunctions {

    public static String getPathToDir() {
        try {
            DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
            domFactory.setNamespaceAware(true); // never forget this!
            DocumentBuilder builder = domFactory.newDocumentBuilder();
            Document doc = builder.parse(new File("fileList.xml"));
            NodeList nodes = doc.getElementsByTagName("mainDirForDeleting");
            String resultString = "";

            resultString = nodes.item(0).getAttributes().getNamedItem("pathToDir").getNodeValue();
            return resultString;
        } catch (Exception ex) {
            LectorUnInstallerApp.logger.fatal(ex.getMessage(), ex);
        }
        return "";
    }

    public static Integer countFiles(String pathToDir) {
        File path = new File(pathToDir);
        Integer x = 0;
        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    x += countFiles(files[i].getAbsolutePath());
                } else {
                    x++;
                }
            }
        }
        return x;
    }
}
