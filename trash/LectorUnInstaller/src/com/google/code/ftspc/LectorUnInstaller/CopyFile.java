/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.google.code.ftspc.LectorUnInstaller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import javax.swing.JOptionPane;

/**
 *
 * @author AKhusnutdinov
 */
public class CopyFile {

    public static void copyfile(String srFile, String dtFile) {
        InputStream in = null;
        try {
            File f1 = new File(srFile);
            File f2 = new File(dtFile);
            in = new FileInputStream(f1);
            OutputStream out = new FileOutputStream(f2);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();

            System.out.println("File copied.");
        } catch (Exception ex) {
            LectorUnInstallerApp.logger.fatal(ex.getMessage(), ex);
                    JOptionPane.showMessageDialog(null, "ERROR"
                + ex.getMessage(), "LectorUnInstaller", 1);
        }
    }
}
