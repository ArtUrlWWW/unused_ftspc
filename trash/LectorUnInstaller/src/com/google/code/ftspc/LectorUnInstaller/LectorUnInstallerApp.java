/*
 * LectorUnInstallerApp.java
 */
package com.google.code.ftspc.LectorUnInstaller;

import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

/**
 * The main class of the application.
 */
public class LectorUnInstallerApp extends SingleFrameApplication {

    public static Logger logger = null;
    public static String shortcutsListContainer;

    /**
     * At startup create and show the main frame of the application.
     */
    @Override
    protected void startup() {

        PropertyConfigurator.configure(props());
        logger = Logger.getRootLogger();

        if (System.getProperty("os.name").toLowerCase().indexOf("win") > -1) {
            shortcutsListContainer = "/shortcuts.win.txt";
        } else {
            shortcutsListContainer = "/shortcuts.txt";
        }

        if (CheckStartFlag.ifExist()) {
            show(new MainFrame());
        } else {
            show(new DeleteFiles());
        }
    }

    private static Properties props() {
        Properties props = new Properties();
        props.put("log4j.rootLogger", "INFO, R");
        props.put("log4j.appender.R",
                "org.apache.log4j.DailyRollingFileAppender");
        props.put("log4j.appender.R.File", "logs/UnInst.log");
        props.put("log4j.appender.R.Append", "true");
        props.put("log4j.appender.R.Threshold", "INFO");
        props.put("log4j.appender.R.DatePattern", "'.'yyyy-MM-dd");
        props.put("log4j.appender.R.layout", "org.apache.log4j.PatternLayout");
        props.put("log4j.appender.R.layout.ConversionPattern",
                //"%d{HH:mm:ss,SSS} %c - %m%n");
                //"[%5p] %d{yyyy-MM-dd mm:ss} (%F:%M:%L)%n%m%n%n");
                "[%5p] %d{yyyy-MM-dd mm:ss} %c (%F:%M:%L)%n%m%n");
        return props;
    }

    /**
     * This method is to initialize the specified window by injecting resources.
     * Windows shown in our application come fully initialized from the GUI
     * builder, so this additional configuration is not needed.
     */
    @Override
    protected void configureWindow(java.awt.Window root) {
    }

    /**
     * A convenient static getter for the application instance.
     * @return the instance of LectorUnInstallerApp
     */
    public static LectorUnInstallerApp getApplication() {
        return Application.getInstance(LectorUnInstallerApp.class);
    }

    /**
     * Main method launching the application.
     */
    public static void main(String[] args) {
        launch(LectorUnInstallerApp.class, args);
    }
}
