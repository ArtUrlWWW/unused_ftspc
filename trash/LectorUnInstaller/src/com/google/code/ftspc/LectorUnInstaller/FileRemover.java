/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.google.code.ftspc.LectorUnInstaller;

import java.io.File;
import java.io.FileInputStream;
import java.util.Scanner;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import org.jdesktop.application.Application;

/**
 *
 * @author AKhusnutdinov
 */
public class FileRemover extends Thread {

    private float countFiles = 0;
    private float countProcessedFiles = 0;

    @Override
    public void run() {
        String pathToDir = CommonFunctions.getPathToDir();

        countFiles = CommonFunctions.countFiles(pathToDir);
        LectorUnInstallerApp.shortcutsListContainer = pathToDir
                + LectorUnInstallerApp.shortcutsListContainer;
        deleteShortcuts();
        deleteFiles(pathToDir);
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                DeleteFiles.jProgressBar1.setValue(100);
            }
        });
        JOptionPane.showMessageDialog(null, "Uninstallation complete. "
                + "\nYou can close the unstaller.", "LectorUnInstaller", 1);
        System.exit(0);
    }

    private void deleteShortcuts() {
        JOptionPane.showMessageDialog(null, "START", "LectorInstaller", 1);
        JOptionPane.showMessageDialog(null, LectorUnInstallerApp.shortcutsListContainer, "LectorInstaller", 1);

        if ((new File(LectorUnInstallerApp.shortcutsListContainer)).exists()) {
            JOptionPane.showMessageDialog(null, "EXIST", "LectorInstaller", 1);
            Scanner scanner = null;
            String tmpStr = "";
            try {
                scanner = new Scanner(new FileInputStream(LectorUnInstallerApp.shortcutsListContainer), "CP1251");
                while (scanner.hasNextLine()) {
                    tmpStr = scanner.nextLine();
                    JOptionPane.showMessageDialog(null, tmpStr, "LectorInstaller", 1);
                    System.out.println((new File(tmpStr)).delete());
                }

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error "
                        + ex.getMessage(), "LectorInstaller", 1);
                LectorUnInstallerApp.logger.fatal(ex.getMessage(), ex);
            } finally {
                try {
                    scanner.close();
                } catch (Exception ex) {
                    LectorUnInstallerApp.logger.fatal(ex.getMessage(), ex);
                }
            }
            JOptionPane.showMessageDialog(null, "DONE", "LectorInstaller", 1);
        }
    }

    private void deleteFiles(String pathToDir) {
        File path = new File(pathToDir);
        int countProgress;
        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteFiles(files[i].getAbsolutePath());
                } else {
                    countProcessedFiles++;
                    countProgress = (int) (countProcessedFiles / countFiles * 100);
                    SwingUtilities.invokeLater(new UpdateProgressBarTask(
                            DeleteFiles.jProgressBar1, countProgress));
                }
                files[i].delete();
            }
            path.delete();
        }
    }

    private class UpdateProgressBarTask implements Runnable {

        private JProgressBar progressBar;
        private int value;

        public UpdateProgressBarTask(JProgressBar progressBar, int value) {
            this.value = value;
            this.progressBar = progressBar;
        }

        @Override
        public void run() {
            if (this.progressBar != null) {
                this.progressBar.setValue(value);
            }
        }
    }
}
