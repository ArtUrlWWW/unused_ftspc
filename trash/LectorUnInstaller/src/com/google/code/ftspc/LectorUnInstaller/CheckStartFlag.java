/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.google.code.ftspc.LectorUnInstaller;

import java.io.File;

/**
 *
 * @author AKhusnutdinov
 */
public class CheckStartFlag {
    public static Boolean ifExist() {
        File startIndicator = new File("LectorInstaller.flag.start");
        return !startIndicator.exists();
    }

}
