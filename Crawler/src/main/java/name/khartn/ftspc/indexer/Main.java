package name.khartn.ftspc.indexer;

import name.khartn.ftspc.indexer.ini_and_vars.Lector_Ini;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import name.khartn.ftspc.indexer.net.crawler.Crawler;

/**
 * Main Class
 *
 * @author Arthur Khusnutdinov
 */
public class Main {
//-Xmx1700m -Xms1500m

    /**
     * Main function
     *
     * @param args Not used.
     */
    public static void main(String[] args) {
        try {
            Lector_Ini.class.newInstance().configure();
            Crawler crawler = new Crawler("http://ihtik.lib.ru/");
//            Crawler crawler = new Crawler("http://bta-kazan.ru/");
            crawler.Main();

        } catch (InstantiationException | IllegalAccessException ex) {
            Vars.logger.fatal("Error: ", ex);
        }
    }
}
