package name.khartn.ftspc.indexer.DataBases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;

/**
 *
 * @author wwwdev
 */
public class HSQLDBHandler {

    static String database = "demobase/demobase";
    public static Connection connection;
//    public static Statement statement = null;
//    public static ResultSet resultSet = null;

    public HSQLDBHandler() {
        try {
            Class.forName("org.hsqldb.jdbcDriver");
            connection = DriverManager.getConnection("jdbc:hsqldb:" + database, "sa", "");
            Statement statement = connection.createStatement();
            try {
                //TODO Добавить поле для названия ссылки. Это нужно будет, например, для забора ссылок на файлы с сайта Ихтика. Определять, нужно ли вставить название ссылки или нет должен плагин для сайта.
                statement.executeUpdate("CREATE cached TABLE IF NOT EXISTS PUBLIC.links ("
                        + "ID INTEGER NOT NULL IDENTITY,"
                        + "link varchar(777) NOT NULL,"
                        + "type varchar(777) DEFAULT 'page',"
                        + "LastModified varchar(777) DEFAULT '',"
                        + "ContentLength varchar(777) DEFAULT '',"
                        + "foundedAt timestamp DEFAULT CURRENT_TIMESTAMP,"
                        + "processedAt timestamp DEFAULT '1900-01-01 00:00:00',"
                        + "processed INTEGER  DEFAULT '0',"
                        + "processedBy varchar(777) DEFAULT ''"
                        + ")");
                
                statement.executeUpdate("create unique index PUBLIC.linksID on PUBLIC.links(ID)");
                statement.executeUpdate("create unique index PUBLIC.linksLink on PUBLIC.links(link)");
                
                statement.executeUpdate("CREATE cached TABLE IF NOT EXISTS PUBLIC.SchedulledLinks ("
                        + "ID INTEGER NOT NULL IDENTITY,"
                        + "link varchar(777) NOT NULL,"
                        + "ParentLink varchar(777)"
                        + ")");
                
                statement.executeUpdate("create unique index PUBLIC.linksIDSL on PUBLIC.SchedulledLinks(ID)");
                statement.executeUpdate("create unique index PUBLIC.linksLinkSL on PUBLIC.SchedulledLinks(link, ParentLink)");

                
            } catch (Exception ex1) {
                ex1.printStackTrace();
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Vars.logger.fatal("Error: ", ex);
        }
    }

    public void insertSingle(String query) {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (SQLException ex) {
            Vars.logger.fatal("Error: "+query, ex);
        }

    }
    
    public Connection getConnection(){
        return connection;
    }
    
    public Integer indexOfLink(String element) {
        try {
            String sql = "SELECT * FROM links where link='" + element + "'";
            Statement statement=connection.createStatement();
            ResultSet resultSetLocal = statement.executeQuery(sql);

            while (resultSetLocal.next()) {
                return resultSetLocal.getInt("ID");

            }
        } catch (SQLException ex) {
            Vars.logger.fatal("Error: ", ex);
        }
        return null;
    }
    public Integer indexOfSchedulledLink(String element) {
        try {
            String sql = "SELECT * FROM SchedulledLinks where link='" + element + "'";
            Statement statement=connection.createStatement();
            ResultSet resultSetLocal = statement.executeQuery(sql);

            while (resultSetLocal.next()) {
                return resultSetLocal.getInt("ID");

            }
        } catch (SQLException ex) {
            Vars.logger.fatal("Error: ", ex);
        }
        return null;
    }
}
