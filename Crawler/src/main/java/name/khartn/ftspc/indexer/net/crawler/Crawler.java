package name.khartn.ftspc.indexer.net.crawler;

import name.khartn.ftspc.indexer.net.crawler.plugins.interfaces.CrawlerPlugin;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import name.khartn.ftspc.indexer.net.crawler.plugins.DefaultPlugin;
import org.apache.commons.lang3.text.WordUtils;

/**
 * Crawler's main class
 *
 * @author Arthur Khusnutdinov
 */
public class Crawler {

    public static String mainURL;
    public static CrawlerPlugin crawlerPlugin = null;

    public Crawler(String mainURL) {
        this.mainURL = mainURL;
    }

    public void Main() {
        /*
         processSite("http://ihtik.lib.ru/2012.03_ihtik_edu/");
         processSite("http://ihtik.lib.ru/2012.03_ihtik_no.big.razd/");
         processSite("http://ihtik.lib.ru/2012.03_ihtik_philos/");
         processSite("http://ihtik.lib.ru/2011.07_ihtik_articles-thesis/");
         processSite("http://ihtik.lib.ru/2012.03_ihtik_dissertat/");
         processSite("http://ihtik.lib.ru/2012.03_ihtik_encycloped/");
         processSite("http://ihtik.lib.ru/2011.06.10_ihtik_history/");
         processSite("http://ihtik.lib.ru/2011.07_ihtik_politolog/");
         processSite("http://ihtik.lib.ru/2011.07_ihtik_sociology/");
         processSite("http://ihtik.lib.ru/2011.06_ihtik_psychol/");
         processSite("http://ihtik.lib.ru/2011.06_ihtik_economic/");
         processSite("http://ihtik.lib.ru/2012.03_ihtik_physic/");
         processSite("http://ihtik.lib.ru/2012.03_ihtik_mathematic/");
         processSite("http://ihtik.lib.ru/ihtik_med_2011.06.12/");
         processSite("http://ihtik.lib.ru/2012.03_ihtik_biology/");
         processSite("http://ihtik.lib.ru/2012.03_ihtik_chem/");
         processSite("http://ihtik.lib.ru/2012.03_ihtik_comp-lib/");
         processSite("http://ihtik.lib.ru/2011.08_ihtik_nauka-tehnika/");
         processSite("http://ihtik.lib.ru/2011.07_ihtik_anytech/");
         processSite("http://ihtik.lib.ru/2012.03_ihtik_radio/");
         processSite("http://ihtik.lib.ru/2011.06_ihtik_stroyka/");
         processSite("http://ihtik.lib.ru/2011.07_ihtik_hudlit-ru/");
         */
        processSite(mainURL);

//        processSite("http://bta-kazan.ru/");
    }

    public void processSite(String siteURL) {
        try {
            mainURL = siteURL;

            Vars.logger.info("Started process of the site " + siteURL + " indexing.");
            String module = siteURL.replace("http://www.", "");
            module = module.replace("http://", "");
            module = module.substring(0, module.indexOf("/"));
            module = WordUtils.capitalize(module);
            try {
                crawlerPlugin = (CrawlerPlugin) Class.forName("name.khartn.ftspc.indexer.net.crawler.plugins." + module.replaceAll("\\.", "_")).newInstance();
            } catch (ClassNotFoundException ex) {
                Vars.logger.info(ex);
            }

            if (Crawler.crawlerPlugin == null) {
                crawlerPlugin = new DefaultPlugin();
            }

            CrawlersProcessPageThread cppt = new CrawlersProcessPageThread(siteURL, crawlerPlugin);
            cppt.start();
//            cppt.processPage(siteURL);

        } catch (InstantiationException | IllegalAccessException ex) {
            Vars.logger.fatal(ex);
        }

    }
}
