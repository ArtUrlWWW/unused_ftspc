package name.khartn.ftspc.indexer.net.crawler;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import name.khartn.ftspc.indexer.net.crawler.plugins.interfaces.CrawlerPlugin;
import name.khartn.ftspc.indexer.net.utils.FileTypesUtil;
import name.khartn.ftspc.indexer.net.utils.ThreadsUtil;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class CrawlersProcessPageThread extends Thread {

    String secondUrl;
     CrawlerPlugin crawlerPlugin;

    public CrawlersProcessPageThread(String secondUrl, CrawlerPlugin crawlerPlugin) {
        this.secondUrl = secondUrl;
        this.crawlerPlugin=crawlerPlugin;
    }

    @Override
    public void run() {
        Vars.threadsCount++;
        this.processPage(secondUrl);
        Vars.threadsCount--;
    }

    public void processPage(String secondUrl) {
        try {

            URL url = new URL(secondUrl);
            URLConnection connection = url.openConnection();
            String sqlQuery;
            ThreadsUtil threadsUtil = new ThreadsUtil();

            if (FileTypesUtil.isHTMLorXML(connection)) {
                Document doc = null;
                String lastModified = "";
                String contentLength = "";
                Connection jsoupConnection = null;
                try {
                    jsoupConnection = Jsoup.connect(secondUrl);
                    if (Vars.useCookies == 1 && Vars.cookies != null) {
                        doc = jsoupConnection.timeout(70000).cookies(Vars.cookies).get();
                    } else {
                        doc = jsoupConnection.timeout(70000).get();
                    }
                } catch (Exception ex) {
//                    Vars.logger.warn("First Error " + secondUrl + " ", ex);
                    try {
                        jsoupConnection = Jsoup.connect(secondUrl);
                        if (Vars.useCookies == 1 && Vars.cookies != null) {
                            doc = jsoupConnection.timeout(70000).cookies(Vars.cookies).get();
                        } else {
                            doc = jsoupConnection.timeout(70000).get();
                        }
                    } catch (Exception ex1) {
                        Vars.logger.warn("Second Error " + secondUrl + " ", ex1);
                    }
                }

                if (doc != null) {

                    if (Vars.useCookies == 1 && Vars.cookies == null) {
                        Vars.cookies = jsoupConnection.response().cookies();
                    }

                    Map<String, String> headers = jsoupConnection.response().headers();
                    contentLength = headers.get("Content-Length");
                    lastModified = headers.get("Last-Modified");

                    if (contentLength == null) {
                        contentLength = "unknown";
                    }
                    if (lastModified == null) {
                        lastModified = "unknown";
                    }

                    Elements links = doc.getElementsByTag("a");
                    String absLink;

                    System.out.println("Page " + secondUrl + " " + this.getId());

                    for (Element link : links) {
                        absLink = link.attr("abs:href");

                        if (!absLink.equals(Crawler.mainURL)
                                && !absLink.equals(Crawler.mainURL + "/")
                                && absLink.contains(Crawler.mainURL)
                                && !crawlerPlugin.excludePage(absLink)) {

                            if (Vars.hSQLDBHandler.indexOfSchedulledLink(absLink) == null
                                    && Vars.hSQLDBHandler.indexOfSchedulledLink(absLink) == null) {
                                sqlQuery = "INSERT INTO SchedulledLinks(link, ParentLink) VALUES ('"
                                        + absLink.replaceAll("'", "\\\\'") + "', '"
                                        + secondUrl.replaceAll("'", "\\\\'") + "');";
                                Vars.hSQLDBHandler.insertSingle(sqlQuery);
                            }
                        }
                    }

                    
                    // TODO Переделать логику работы с SchedulledLinks - создать отдельный воркер, например, на quartz
                    java.sql.Connection dbConn;
                    dbConn = Vars.hSQLDBHandler.getConnection();
                    String sql = "SELECT * FROM SchedulledLinks where ParentLink='" + secondUrl.replaceAll("'", "\\\\'") + "'";
                    Statement statement = dbConn.createStatement();
                    ResultSet resultSetLocal = statement.executeQuery(sql);

                    while (resultSetLocal.next()) {
                        absLink = resultSetLocal.getString("link");

                        threadsUtil.waitForThreads();

                        CrawlersProcessPageThread cppt = new CrawlersProcessPageThread(absLink, crawlerPlugin);
                        cppt.start();
                    }

                    crawlerPlugin.processPage(doc);

//                    sqlQuery = "delete from SchedulledLinks where link='"
//                            + secondUrl.replaceAll("'", "\\\\'") + "';";
//                    Vars.hSQLDBHandler.insertSingle(sqlQuery);
                }
                doc = null;

                sqlQuery = "INSERT INTO links(link, LastModified, ContentLength, type) VALUES ('" + secondUrl + "', '" + lastModified + "', '" + contentLength + "', 'page' );";
                Vars.hSQLDBHandler.insertSingle(sqlQuery);
            } else {
                sqlQuery = "INSERT INTO links(link, type) VALUES ('" + secondUrl + "', 'unknown');";
                Vars.hSQLDBHandler.insertSingle(sqlQuery);
            }

        } catch (SQLException ex) {
            Vars.logger.fatal("Error: ", ex);
        } catch (IOException ex) {
            Vars.logger.fatal("Error: ", ex);
        }
    }
}
