package name.khartn.ftspc.indexer.net.crawler.plugins;


import name.khartn.ftspc.indexer.net.crawler.plugins.interfaces.CrawlerPlugin;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class Ihtik_lib_ru implements CrawlerPlugin {

    @Override
    public void processPage(Document doc) {
        try {
            String pageUrl = doc.baseUri();

            Elements tables = doc.getElementsByTag("table");
            if (tables.size() > 0) {
                String[] tmpStrArr;
                tmpStrArr = pageUrl.replace("http://", "").split("/");
                pageUrl += "/" + tmpStrArr[1] + "_";
                String link;

                for (Element table : tables) {
                    Elements trs = table.getElementsByTag("tr");
                    for (Element tr : trs) {
                        if (tr.attr("class").equals("e") || tr.attr("class").equals("")) {
                            Elements tds = tr.getElementsByTag("td");
                            Integer counter = 0;
                            for (Element td : tds) {
                                counter++;
                                if (counter < 2) {
                                    tmpStrArr = td.text().split("\\.");
                                    link = pageUrl + tmpStrArr[0] + ".rar";
                                    if (Vars.hSQLDBHandler.indexOfLink(link) == null) {

                                        String sqlQuery = "INSERT INTO links(link, type) VALUES ('" + link + "', 'unknown');";
                                        Vars.hSQLDBHandler.insertSingle(sqlQuery);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Vars.logger.fatal("Error: ", ex);
        }
    }

    @Override
    public Boolean excludePage(String url) {
        if (url.contains("_catalog_ihtik.lib.ru_")) {
            return true;
        } else {
            return false;
        }
    }
}
