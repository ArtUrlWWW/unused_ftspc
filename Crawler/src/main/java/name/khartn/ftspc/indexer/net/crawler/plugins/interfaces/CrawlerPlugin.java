package name.khartn.ftspc.indexer.net.crawler.plugins.interfaces;

import org.jsoup.nodes.Document;

/**
 *
 * @author Arthur Khusnutdinov
 */
public interface CrawlerPlugin {    
    public void processPage(Document doc);
    
    public Boolean excludePage(String url);
    
}
